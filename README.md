# mjinfo
A collection of utilites to play richii (actual, not the single player game) mahjong.

Meant as a demo project and to study Vue.js, Typescript and Unit Testing.

Special thanks to "FluffyStuff" for the vector gaphics
https://github.com/FluffyStuff/riichi-mahjong-tiles

## Notable directories
/src/lib/mahjong/ A representation of various rules of mahjong in typescript
/src/assets/sass/ Sass project wide variables
/src/assets/language i18n support
/src/tests/ Unit tests
/src/views/calculator A complex array of .vue components to allow score calculation

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```
