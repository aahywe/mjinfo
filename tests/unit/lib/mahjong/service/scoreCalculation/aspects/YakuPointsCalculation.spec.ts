// TODO
// // TODO: Write tests with random values
//
// import { expect } from 'chai';
// import 'mocha';
//
// import YakuPointsCalculation from "@/lib/mahjong/service/scoreCalculation/aspects/YakuPointsCalculation";
// import SevenPairsHandConfiguration from "@/lib/mahjong/model/suiteSet/SevenPairsHandConfiguration";
// import Tile from "@/lib/mahjong/model/Tile";
// import Pair from "@/lib/mahjong/model/suites/Pair";
// import WinningPoints, {TYakuType} from "../../../../../../../src/lib/mahjong/model/scoring/WinningPoints";
// import NormalHandConfiguration from "@/lib/mahjong/model/suiteSet/NormalHandConfiguration";
// import Street from "@/lib/mahjong/model/suites/Street";
// import Triplet from "@/lib/mahjong/model/suites/Triplet";
// import Quad from "@/lib/mahjong/model/suites/Quad";
//
// function countYakuOfType(yakuPoints: Array<WinningPoints>, yakuType: TYakuType): number {
//     let count: number = 0;
//     for (let yakuPoint of yakuPoints) {
//         if (yakuPoint.getType() === yakuType) {
//             count++;
//         }
//     }
//
//     return count;
// }
//
// function getFirstYakuOfType(yakuPoints: Array<WinningPoints>, yakuType: TYakuType): WinningPoints {
//     for (let yakuPoint of yakuPoints) {
//         if (yakuPoint.getType() === yakuType) {
//             return yakuPoint;
//         }
//     }
//
//     throw new RangeError("getFirstYakuOfType failed");
// }
//
// describe('YakuPointsCalculation', () => {
//    it('should detect akai dora tiles', () => {
//        let sampleHand = new SevenPairsHandConfiguration(
//            [
//                new Pair(
//                    new Tile('man', '5', false),
//                    new Tile('man', '5', true)
//                ),
//                new Pair(
//                    new Tile('bamboo', '9', false),
//                    new Tile('bamboo', '9', false)
//                ),
//                new Pair(
//                    new Tile('wind', 'east', false),
//                    new Tile('wind', 'east', false)
//                ),
//                new Pair(
//                    new Tile('dragon', 'green', false),
//                    new Tile('dragon', 'green', false)
//                ),
//                new Pair(
//                    new Tile('pin', '5', false),
//                    new Tile('pin', '5', false)
//                ),
//                new Pair(
//                    new Tile('pin', '5', false),
//                    new Tile('pin', '5', false)
//                ),
//                new Pair(
//                    new Tile('bamboo', '5', true),
//                    new Tile('bamboo', '5', false)
//                ),
//            ]
//        );
//        let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//            [],
//            [],
//            true,
//            false,
//            false
//        );
//        expect(countYakuOfType(result, 'YAKU_DORA_AKAI')).to.equal(2);
//
//        let sampleHand2 = new SevenPairsHandConfiguration(
//            [
//                new Pair(
//                    new Tile('man', '5', false),
//                    new Tile('man', '5', false)
//                ),
//                new Pair(
//                    new Tile('bamboo', '9', false),
//                    new Tile('bamboo', '9', false)
//                ),
//                new Pair(
//                    new Tile('wind', 'east', false),
//                    new Tile('wind', 'east', false)
//                ),
//                new Pair(
//                    new Tile('dragon', 'green', false),
//                    new Tile('dragon', 'green', false)
//                ),
//                new Pair(
//                    new Tile('pin', '5', false),
//                    new Tile('pin', '5', false)
//                ),
//                new Pair(
//                    new Tile('pin', '5', false),
//                    new Tile('pin', '5', false)
//                ),
//                new Pair(
//                    new Tile('bamboo', '5', false),
//                    new Tile('bamboo', '5', false)
//                ),
//            ]
//        );
//        let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//            [],
//            [],
//            true,
//            false,
//            false
//        );
//        expect(countYakuOfType(result2, 'YAKU_DORA_AKAI')).to.equal(0);
//    });
//
//     it ('should detect dora tiles', () => {
//         let sampleHand = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', true)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', true),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_DORA')).to.equal(6);
//
//         let sampleHand2 = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [
//                 new Tile('wind', 'north', false),
//                 new Tile('pin', '4', false),
//                 new Tile('pin', '4', false),
//             ],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_DORA')).to.equal(10);
//     });
//
//     it ('should detect ura dora tiles', () => {
//         let sampleHand = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', true)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', true),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_DORA_URA')).to.equal(6);
//
//         let sampleHand2 = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [
//                 new Tile('wind', 'north', false),
//                 new Tile('pin', '4', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_DORA_URA')).to.equal(10);
//
//         let sampleHand3 = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result3 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [
//                 new Tile('wind', 'north', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result3, 'YAKU_DORA_URA')).to.equal(0);
//     });
//
//     it ('should detect seven pairs yaku', () => {
//         let sampleHand = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', true)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', true),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_SEVEN_PAIRS')).to.equal(1);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('man', '5', false),
//                 new Tile('man', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     true
//                 ),
//                 new Triplet(
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     true
//                 ),
//             ],
//             []
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_SEVEN_PAIRS')).to.equal(0);
//     });
//
//     it ('should detect richii', () => {
//         let sampleHand = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', true)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', true),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_RICHII')).to.equal(1);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('man', '5', false),
//                 new Tile('man', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     true
//                 ),
//                 new Triplet(
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     true
//                 ),
//             ],
//             []
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_RICHII')).to.equal(0);
//     });
//
//     it ('should detect mensen tsumo', () => {
//         let sampleHand = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('man', '5', false),
//                 new Tile('man', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     false
//                 ),
//             ],
//             []
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_MENSEN_TSUMO')).to.equal(1);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('man', '5', false),
//                 new Tile('man', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     true
//                 ),
//                 new Triplet(
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     true
//                 ),
//             ],
//             []
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_MENSEN_TSUMO')).to.equal(0);
//     });
//
//     it ('should detect ippatsu', () => {
//         let sampleHand = new SevenPairsHandConfiguration(
//             [
//                 new Pair(
//                     new Tile('man', '5', false),
//                     new Tile('man', '5', true)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false)
//                 ),
//                 new Pair(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false)
//                 ),
//                 new Pair(
//                     new Tile('dragon', 'green', false),
//                     new Tile('dragon', 'green', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false)
//                 ),
//                 new Pair(
//                     new Tile('bamboo', '5', true),
//                     new Tile('bamboo', '5', false)
//                 ),
//             ]
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             [
//                 new Tile('dragon', 'white', false),
//                 new Tile('pin', '4', false),
//             ],
//             true,
//             true,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_IPPATSU')).to.equal(0);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('man', '5', false),
//                 new Tile('man', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     new Tile('pin', '5', false),
//                     true
//                 ),
//                 new Triplet(
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '2', false),
//                     true
//                 ),
//             ],
//             []
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             true,
//             true
//         );
//         expect(countYakuOfType(result2, 'YAKU_IPPATSU')).to.equal(1);
//     });
//
//     it ('should detect honitsu / chinitsu', () => {
//         let sampleHand = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     true
//                 ),
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_HONITSU')).to.equal(1);
//         expect(countYakuOfType(result, 'YAKU_CHINITSU')).to.equal(0);
//         expect(getFirstYakuOfType(result, 'YAKU_HONITSU').getScore() === 2);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     new Tile('wind', 'east', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     false
//                 ),
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     false
//                 ),
//             ],
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_HONITSU')).to.equal(1);
//         expect(countYakuOfType(result, 'YAKU_CHINITSU')).to.equal(0);
//         expect(getFirstYakuOfType(result, 'YAKU_HONITSU').getScore() === 3);
//
//         let sampleHand3 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     true
//                 ),
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result3 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand3,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result3, 'YAKU_HONITSU')).to.equal(0);
//         expect(countYakuOfType(result3, 'YAKU_CHINITSU')).to.equal(1);
//         expect(getFirstYakuOfType(result3, 'YAKU_CHINITSU').getScore() === 5);
//
//         let sampleHand4 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '9', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     true
//                 ),
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result4 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand4,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result4, 'YAKU_HONITSU')).to.equal(0);
//         expect(countYakuOfType(result4, 'YAKU_CHINITSU')).to.equal(1);
//         expect(getFirstYakuOfType(result4, 'YAKU_CHINITSU').getScore() === 6);
//
//         let sampleHand5 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '1', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     false
//                 )
//             ],
//             [
//                 new Triplet(
//                     new Tile('man', '9', false),
//                     new Tile('man', '9', false),
//                     new Tile('man', '9', false),
//                     false
//                 ),
//                 new Triplet(
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '5', false),
//                     true
//                 ),
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result5 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand5,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result5, 'YAKU_HONITSU')).to.equal(0);
//         expect(countYakuOfType(result5, 'YAKU_CHINITSU')).to.equal(0);
//     });
//
//     it ('should detect ittsu', () => {
//         let sampleHand = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '7', false),
//                     new Tile('bamboo', '8', false),
//                     new Tile('bamboo', '9', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '4', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '6', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//             ],
//             [
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     false
//                 ),
//             ],
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_ITTSU')).to.equal(1);
//         expect(getFirstYakuOfType(result, 'YAKU_ITTSU').getScore() === 2);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '7', false),
//                     new Tile('bamboo', '8', false),
//                     new Tile('bamboo', '9', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '4', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '6', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//             ],
//             [
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_ITTSU')).to.equal(1);
//         expect(getFirstYakuOfType(result2, 'YAKU_ITTSU').getScore() === 1);
//
//         let sampleHand3 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('pin', '7', false),
//                     new Tile('pin', '8', false),
//                     new Tile('pin', '9', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '4', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '6', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//             ],
//             [
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     false
//                 ),
//             ],
//         );
//         let result3 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand3,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result3, 'YAKU_ITTSU')).to.equal(0);
//     });
//
//     it ('should detect ippeku', () => {
//         let sampleHand = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '7', false),
//                     new Tile('bamboo', '8', false),
//                     new Tile('bamboo', '9', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '9', false),
//                     new Tile('bamboo', '7', false),
//                     new Tile('bamboo', '8', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//             ],
//             [
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_IPPEKU')).to.equal(0);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '4', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '6', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//             ],
//             [
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     false
//                 ),
//             ],
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_IPPEKU')).to.equal(1);
//     });
//
//     it ('should detect tanyao', () => {
//         let sampleHand = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('bamboo', '5', false),
//                 new Tile('bamboo', '5', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '6', false),
//                     new Tile('bamboo', '7', false),
//                     new Tile('bamboo', '8', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('pin', '2', false),
//                     new Tile('pin', '3', false),
//                     new Tile('pin', '4', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '4', false),
//                     false
//                 ),
//             ],
//             [
//                 new Triplet(
//                     new Tile('bamboo', '6', false),
//                     new Tile('bamboo', '6', false),
//                     new Tile('bamboo', '6', false),
//                     true
//                 ),
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     true
//                 ),
//             ],
//         );
//         let result = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result, 'YAKU_TANYAO')).to.equal(1);
//
//         let sampleHand2 = new NormalHandConfiguration(
//             new Pair(
//                 new Tile('dragon', 'green', false),
//                 new Tile('dragon', 'green', false)
//             ),
//             [
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '4', false),
//                     new Tile('bamboo', '5', false),
//                     new Tile('bamboo', '6', false),
//                     false
//                 ),
//                 new Street(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '3', false),
//                     new Tile('bamboo', '1', false),
//                     false
//                 ),
//             ],
//             [
//             ],
//             [
//                 new Quad(
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     new Tile('bamboo', '2', false),
//                     false
//                 ),
//             ],
//         );
//         let result2 = YakuPointsCalculation.getWinningPointsForHandConfiguration(sampleHand2,
//             [],
//             [],
//             true,
//             false,
//             false
//         );
//         expect(countYakuOfType(result2, 'YAKU_TANYAO')).to.equal(0);
//     });
//
//     // TODO: TerminalInEachSuit / TerminalOrHonorInEachSet
// });
