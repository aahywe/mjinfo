import { expect } from 'chai';
import 'mocha';

import Tile from "@/lib/mahjong/model/Tile";
import Hand from "@/lib/mahjong/model/Hand";

import SevenPairsHandSuiteSetDetection from "@/lib/mahjong/service/scoreCalculation/suiteSetDetection/SevenPairsHandSuiteSetDetection";

describe('SevenPairHandsDetector', () => {

    it ('Should detect valid seven pair hands', () => {
        let result = SevenPairsHandSuiteSetDetection.tryBuildSevenPairsHand(
            new Hand(
                [
                    new Tile('man', '5', true),
                    new Tile('man', '5', false),
                    new Tile('pin', '9', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '3', false),
                    new Tile('pin', '3', false),
                    new Tile('pin', '1', false),
                    new Tile('pin', '1', false),
                ],
                [],
                [],
                [],
                [],
                new Tile('pin', '9', false)
            ),
        );

        expect(result).to.not.equal(null);

        let result2 = SevenPairsHandSuiteSetDetection.tryBuildSevenPairsHand(
            new Hand(
                [
                    new Tile('man', '5', true),
                    new Tile('man', '5', false),
                    new Tile('pin', '9', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '3', false),
                    new Tile('pin', '3', false),
                    new Tile('pin', '3', false),
                    new Tile('pin', '1', false),
                ],
                [],
                [],
                [],
                [],
                new Tile('pin', '9', false)
            )
        );

        expect(result2).to.equal(null);
    });

});