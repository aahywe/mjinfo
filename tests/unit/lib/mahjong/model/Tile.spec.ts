import { expect } from 'chai';
import 'mocha';
import Tile, {TColor, TNumber} from "@/lib/mahjong/model/Tile";

describe('Tile', () => {

    let numberSimples: Array<TNumber> = ['2', '3', '4', '5', '6', '7', '8'];
    let numberTerminals: Array<TNumber> = ['1', '9'];
    let numberDragons: Array<TNumber> = ['red', 'white', 'green'];
    let numberWinds: Array<TNumber> = ['east', 'south', 'west', 'north'];
    let numberHonors: Array<TNumber> = numberWinds.concat(numberDragons);
    let numbers: Array<TNumber> = numberSimples.concat(numberTerminals).concat(numberDragons).concat(numberWinds);

    let colors = ['pin', 'man', 'bamboo', 'wind', 'dragon'];

    it('should tell you if a color / number / red combination is valid', () => {
        for (let numberSimple of numberSimples) {
            expect(Tile.isValidCombinationForTile('pin', numberSimple, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('man', numberSimple, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('bamboo', numberSimple, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('wind', numberSimple, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberSimple, false)).to.equal(false);

            expect(Tile.isValidCombinationForTile('pin', numberSimple, true)).to.equal(numberSimple === '5');
            expect(Tile.isValidCombinationForTile('man', numberSimple, true)).to.equal(numberSimple === '5');
            expect(Tile.isValidCombinationForTile('bamboo', numberSimple, true)).to.equal(numberSimple === '5');
            expect(Tile.isValidCombinationForTile('wind', numberSimple, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberSimple, true)).to.equal(false);
        }

        for (let numberTerminal of numberTerminals) {
            expect(Tile.isValidCombinationForTile('pin', numberTerminal, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('man', numberTerminal, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('bamboo', numberTerminal, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('wind', numberTerminal, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberTerminal, false)).to.equal(false);

            expect(Tile.isValidCombinationForTile('pin', numberTerminal, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('man', numberTerminal, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('bamboo', numberTerminal, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('wind', numberTerminal, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberTerminal, true)).to.equal(false);
        }

        for (let numberDragon of numberDragons) {
            expect(Tile.isValidCombinationForTile('pin', numberDragon, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('man', numberDragon, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('bamboo', numberDragon, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('wind', numberDragon, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberDragon, false)).to.equal(true);

            expect(Tile.isValidCombinationForTile('pin', numberDragon, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('man', numberDragon, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('bamboo', numberDragon, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('wind', numberDragon, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberDragon, true)).to.equal(false);
        }

        for (let numberWind of numberWinds) {
            expect(Tile.isValidCombinationForTile('pin', numberWind, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('man', numberWind, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('bamboo', numberWind, false)).to.equal(false);
            expect(Tile.isValidCombinationForTile('wind', numberWind, false)).to.equal(true);
            expect(Tile.isValidCombinationForTile('dragon', numberWind, false)).to.equal(false);

            expect(Tile.isValidCombinationForTile('pin', numberWind, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('man', numberWind, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('bamboo', numberWind, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('wind', numberWind, true)).to.equal(false);
            expect(Tile.isValidCombinationForTile('dragon', numberWind, true)).to.equal(false);
        }
    });

    it('should only allowed to be constructed with a valid color / number / red combination', () => {
        for (let numberSimple of numberSimples) {
            expect(()=> {new Tile('pin', numberSimple, false)}).to.not.throw();
            expect(()=> {new Tile('man', numberSimple, false)}).to.not.throw();
            expect(()=> {new Tile('bamboo', numberSimple, false)}).to.not.throw();
            expect(()=> {new Tile('wind', numberSimple, false)}).to.throw();
            expect(()=> {new Tile('dragon', numberSimple, false)}).to.throw();

            if (numberSimple === '5') {
                expect(()=> {new Tile('pin', numberSimple, true)}).to.not.throw();
                expect(()=> {new Tile('man', numberSimple, true)}).to.not.throw();
                expect(()=> {new Tile('bamboo', numberSimple, true)}).to.not.throw();
            } else {
                expect(()=> {new Tile('pin', numberSimple, true)}).to.throw();
                expect(()=> {new Tile('man', numberSimple, true)}).to.throw();
                expect(()=> {new Tile('bamboo', numberSimple, true)}).to.throw();
            }

            expect(()=> {new Tile('wind', numberSimple, true)}).to.throw();
            expect(()=> {new Tile('dragon', numberSimple, true)}).to.throw();
        }

        for (let numberTerminal of numberTerminals) {
            expect(()=> {new Tile('pin', numberTerminal, false)}).to.not.throw();
            expect(()=> {new Tile('man', numberTerminal, false)}).to.not.throw();
            expect(()=> {new Tile('bamboo', numberTerminal, false)}).to.not.throw();
            expect(()=> {new Tile('wind', numberTerminal, false)}).to.throw();
            expect(()=> {new Tile('dragon', numberTerminal, false)}).to.throw();

            expect(()=> {new Tile('pin', numberTerminal, true)}).to.throw();
            expect(()=> {new Tile('man', numberTerminal, true)}).to.throw();
            expect(()=> {new Tile('bamboo', numberTerminal, true)}).to.throw();
            expect(()=> {new Tile('wind', numberTerminal, true)}).to.throw();
            expect(()=> {new Tile('dragon', numberTerminal, true)}).to.throw();
        }

        for (let numberDragon of numberDragons) {
            expect(()=> {new Tile('pin', numberDragon, false)}).to.throw();
            expect(()=> {new Tile('man', numberDragon, false)}).to.throw();
            expect(()=> {new Tile('bamboo', numberDragon, false)}).to.throw();
            expect(()=> {new Tile('wind', numberDragon, false)}).to.throw();
            expect(()=> {new Tile('dragon', numberDragon, false)}).to.not.throw();

            expect(()=> {new Tile('pin', numberDragon, true)}).to.throw();
            expect(()=> {new Tile('man', numberDragon, true)}).to.throw();
            expect(()=> {new Tile('bamboo', numberDragon, true)}).to.throw();
            expect(()=> {new Tile('wind', numberDragon, true)}).to.throw();
            expect(()=> {new Tile('dragon', numberDragon, true)}).to.throw();
        }

        for (let numberWind of numberWinds) {
            expect(()=> {new Tile('pin', numberWind, false)}).to.throw();
            expect(()=> {new Tile('man', numberWind, false)}).to.throw();
            expect(()=> {new Tile('bamboo', numberWind, false)}).to.throw();
            expect(()=> {new Tile('wind', numberWind, false)}).to.not.throw();
            expect(()=> {new Tile('dragon', numberWind, false)}).to.throw();

            expect(()=> {new Tile('pin', numberWind, true)}).to.throw();
            expect(()=> {new Tile('man', numberWind, true)}).to.throw();
            expect(()=> {new Tile('bamboo', numberWind, true)}).to.throw();
            expect(()=> {new Tile('wind', numberWind, true)}).to.throw();
            expect(()=> {new Tile('dragon', numberWind, true)}).to.throw();
        }
    });

    it('should memorize his number, color and red five status', ()=> {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile: Tile = new Tile(<TColor>color, number, false);
                    expect(tile.tileColor).to.equal(color);
                    expect(tile.tileNumber).to.equal(number);
                    expect(tile.isRedFive).to.equal(false);
                }

                if (Tile.isValidCombinationForTile(<TColor>color, number, true)) {
                    let tile: Tile = new Tile(<TColor>color, number, true);
                    expect(tile.tileColor).to.equal(color);
                    expect(tile.tileNumber).to.equal(number);
                    expect(tile.isRedFive).to.equal(true);
                }
            }
        }
    });

    it('should deduct if it is a simple, terminal or honor type', ()=> {
        for (let color of colors) {
            for (let number of numberSimples) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile: Tile = new Tile(<TColor>color, number, false);
                    expect(tile.tileType).to.equal('simple');
                }

                if (Tile.isValidCombinationForTile(<TColor>color, number, true)) {
                    let tile: Tile = new Tile(<TColor>color, number, false);
                    expect(tile.tileType).to.equal('simple');
                }
            }

            for (let numberTerminal of numberTerminals) {
                if (Tile.isValidCombinationForTile(<TColor>color, numberTerminal, false)) {
                    let tile: Tile = new Tile(<TColor>color, numberTerminal, false);
                    expect(tile.tileType).to.equal('terminal');
                }

                if (Tile.isValidCombinationForTile(<TColor>color, numberTerminal, true)) {
                    let tile: Tile = new Tile(<TColor>color, numberTerminal, false);
                    expect(tile.tileType).to.equal('terminal');
                }
            }

            for (let numberHonor of numberHonors) {
                if (Tile.isValidCombinationForTile(<TColor>color, numberHonor, false)) {
                    let tile: Tile = new Tile(<TColor>color, numberHonor, false);
                    expect(tile.tileType).to.equal('honor');
                }

                if (Tile.isValidCombinationForTile(<TColor>color, numberHonor, true)) {
                    let tile: Tile = new Tile(<TColor>color, numberHonor, false);
                    expect(tile.tileType).to.equal('honor');
                }
            }
        }
    })
});