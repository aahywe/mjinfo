import { expect } from 'chai';
import 'mocha';
import Tile, {TColor, TNumber} from "@/lib/mahjong/model/Tile";
import Quad from "@/lib/mahjong/model/suites/Quad";

describe('Pon', () => {

    let numberSimples: Array<TNumber> = ['2', '3', '4', '5', '6', '7', '8'];
    let numberTerminals: Array<TNumber> = ['1', '9'];
    let numberDragons: Array<TNumber> = ['red', 'white', 'green'];
    let numberWinds: Array<TNumber> = ['east', 'south', 'west', 'north'];
    let numberHonors: Array<TNumber> = numberWinds.concat(numberDragons);
    let numbers: Array<TNumber> = numberSimples.concat(numberTerminals).concat(numberDragons).concat(numberWinds);

    let colors = ['pin', 'man', 'bamboo', 'wind', 'dragon'];

    it('should not be constructed with duplicate tiles', () => {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile = new Tile(<TColor>color, number, false);
                    expect(() => {
                        new Quad(tile, tile, tile, tile, false);
                    }).to.throw();
                    expect(() => {
                        new Quad(tile, tile, tile, tile, true);
                    }).to.throw();
                }
            }
        }
    });

    it('should only be constructed with identical tiles', () => {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile1 = new Tile(<TColor>color, number, false);
                    let tile2 = new Tile(<TColor>color, number, false);
                    let tile3 = new Tile(<TColor>color, number, false);
                    let tile4 = new Tile(<TColor>color, number, false);
                    expect(() => {
                        let newKan = new Quad(tile1, tile2, tile3, tile4, false);

                        expect(newKan.firstTile === tile1).to.equal(true);
                        expect(newKan.secondTile === tile2).to.equal(true);
                        expect(newKan.thirdTile === tile3).to.equal(true);
                        expect(newKan.fourthTile === tile4).to.equal(true);
                        expect(newKan.isOpen).to.equal(false);
                    }).to.not.throw();
                    expect(() => {
                        let newKan = new Quad(tile1, tile2, tile3, tile4, true);

                        expect(newKan.firstTile === tile1).to.equal(true);
                        expect(newKan.secondTile === tile2).to.equal(true);
                        expect(newKan.thirdTile === tile3).to.equal(true);
                        expect(newKan.fourthTile === tile4).to.equal(true);
                        expect(newKan.isOpen).to.equal(true);
                    }).to.not.throw();

                    if (number === '5') {
                        let tile5 = new Tile(<TColor>color, number, true);
                        let tile6 = new Tile(<TColor>color, number, false);
                        let tile7 = new Tile(<TColor>color, number, false);
                        let tile8 = new Tile(<TColor>color, number, false);
                        expect(() => {
                            let newKan = new Quad(tile5, tile6, tile7, tile8, false);

                            expect(newKan.firstTile === tile5).to.equal(true);
                            expect(newKan.secondTile === tile6).to.equal(true);
                            expect(newKan.thirdTile === tile7).to.equal(true);
                            expect(newKan.fourthTile === tile8).to.equal(true);
                            expect(newKan.isOpen).to.equal(false);
                        }).to.not.throw();

                        expect(() => {
                            let newKan = new Quad(tile5, tile6, tile7, tile8, true);

                            expect(newKan.firstTile === tile5).to.equal(true);
                            expect(newKan.secondTile === tile6).to.equal(true);
                            expect(newKan.thirdTile === tile7).to.equal(true);
                            expect(newKan.fourthTile === tile8).to.equal(true);
                            expect(newKan.isOpen).to.equal(true);
                        }).to.not.throw();
                    }
                }
            }
        }

        expect(() => {
            new Quad(
                new Tile('man', '5', true),
                new Tile('man', '5', false),
                new Tile('man', '6', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Quad(
                new Tile('man', '5', false),
                new Tile('man', '6', false),
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Quad(
                new Tile('man', '6', false),
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Quad(
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                new Tile('man', '6', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Quad(
                new Tile('man', '5', false),
                new Tile('pin', '5', false),
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();
    });
});