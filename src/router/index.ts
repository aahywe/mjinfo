import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home.vue'
import Calculator from "@/views/Calculator.vue";
import WinningHands from "@/views/WinningHands.vue";
import ScoreTable from "@/views/ScoreTable.vue";
import Language from "@/assets/i18n/Language";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: Language.getInstance().get('APP_NAV_HOME'),
        component: Home
    },
    {
        path: '/winning-hands',
        name: Language.getInstance().get('APP_NAV_WINNING_HANDS'),
        component: WinningHands
    },
    {
        path: '/score-table',
        name: Language.getInstance().get('APP_NAV_SCORE_TABLE'),
        component: ScoreTable
    },
    {
        path: '/calculator',
        name: Language.getInstance().get('APP_NAV_CALCULATOR'),
        component: Calculator
    },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
