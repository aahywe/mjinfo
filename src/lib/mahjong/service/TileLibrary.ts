import Tile, {TColor, TNumber} from "@/lib/mahjong/model/Tile";
import {allValidColorsAndNumbers} from "@/lib/mahjong/constants/AllValidColorsAndNumbers";

export type TTilesOfColor = {colorName: TColor, numbers: Array<{number: TNumber, isRed: boolean, tiles: Array<Tile>}>};

export default class TileLibrary {
    private tileList: Array<TTilesOfColor>;

    public get(): Array<TTilesOfColor> {
        return this.tileList;
    }

    constructor(withRedFive: boolean) {
        let tilesList: Array<TTilesOfColor> = [];

        for (let colorInfo of allValidColorsAndNumbers) {
            let tilesOfColor: TTilesOfColor = {
                colorName: colorInfo.color,
                numbers: []
            };

            for (let number of colorInfo.numbers) {
                let numberInfo: {number: TNumber, isRed: boolean, tiles: Array<Tile>} =
                    {number: number, isRed: false, tiles: []};

                let countOfSameTiles: number = 4;
                if (withRedFive && number === '5') {
                    countOfSameTiles = 3;
                }

                for (let i = 1; i <= countOfSameTiles; i++) {
                    numberInfo.tiles.push(new Tile(
                        colorInfo.color,
                        number,
                        false
                    ));
                }
                tilesOfColor.numbers.push(numberInfo);

                if (withRedFive && number === '5') {
                    tilesOfColor.numbers.push({
                        number: number,
                        isRed: true,
                        tiles: [
                            new Tile(colorInfo.color, number, true)
                        ]
                    });
                }
            }

            tilesList.push(tilesOfColor);
        }

        this.tileList = tilesList;
    }
}