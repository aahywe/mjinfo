import Hand from "@/lib/mahjong/model/Hand";
import Tile from "@/lib/mahjong/model/Tile";

import Pair from "@/lib/mahjong/model/suites/Pair";
import Quad from "@/lib/mahjong/model/suites/Quad";
import Triplet from "@/lib/mahjong/model/suites/Triplet";
import Street from "@/lib/mahjong/model/suites/Street";

import StandardSuiteSet from "@/lib/mahjong/model/suiteSet/StandardSuiteSet";

export default class StandardSuiteSetDetection {
    /**
     * Returns all pair suites that can be crafted from the tiles
     * @param tiles
     */
    private static getPossiblePairs(tiles: Array<Tile>): Array<Pair> {
        let pairs: Array<Pair> = [];

        for (let i = 0; i < tiles.length; i++) {
            for (let i2 = i + 1; i2 < tiles.length; i2++) {
                if (
                    tiles[i].tileColor === tiles[i2].tileColor
                    && tiles[i].tileNumber === tiles[i2].tileNumber
                ) {
                    pairs.push(new Pair(tiles[i], tiles[i2]));
                }
            }
        }

        return pairs;
    }

    /**
     * Returns all streets that can be crafted from the tiles
     * @param tiles
     */
    private static getPossibleStreets(tiles: Array<Tile>): Array<Street> {
        let streets: Array<Street> = [];

        for (let i = 0; i < tiles.length; i++) {
            for (let i2 = i + 1; i2 < tiles.length; i2++) {
                for (let i3 = i2 + 1; i3 < tiles.length; i3++) {
                    if (Street.isValidStreet(tiles[i], tiles[i2], tiles[i3])) {
                        streets.push(new Street(tiles[i], tiles[i2], tiles[i3], false));
                    }
                }
            }
        }

        return streets;
    }

    /**
     * Returns all closed triplets that can be created from the tiles
     * @param tiles
     */
    private static getPossibleClosedTriplets(tiles: Array<Tile>): Array<Triplet> {
        let triplets: Array<Triplet> = [];

        for (let i = 0; i < tiles.length; i++) {
            for (let i2 = i + 1; i2 < tiles.length; i2++) {
                for (let i3 = i2 + 1; i3 < tiles.length; i3++) {
                    if (Triplet.isValidPon(tiles[i], tiles[i2], tiles[i3])) {
                        triplets.push(new Triplet(tiles[i], tiles[i2], tiles[i3], false));
                    }
                }
            }
        }

        return triplets;
    }

    /**
     * Recursive function to find all combinations of triplets/streets where all tiles
     * have been put into without the tiles overlaping in two suites at the same time
     * @param streetsAndTripletsThisBranch - Combo created so far this branch
     * @param streetsAndTripletsThatCanBeAdded - Triplets or streets that could be added here
     * @param pair - The pair used here
     * @param amountOfStreetsAndTripletsRequired - The amount of streets/triplets that required to be completed
     */
    private static getPermutations(
        streetsAndTripletsThisBranch: Array<Street|Triplet>,
        streetsAndTripletsThatCanBeAdded: Array<Street|Triplet>,
        pair: Pair,
        amountOfStreetsAndTripletsRequired: number
    ): Array<Array<Street|Triplet>> {
        if (amountOfStreetsAndTripletsRequired === 0) {
            // end point reached. This is a possible solution
            return [streetsAndTripletsThisBranch];
        }

        let results: Array<Array<Street|Triplet>> = [];
        // try out for all streets/triplets if we can add them at this branch
        for (let i = 0; i < streetsAndTripletsThatCanBeAdded.length; i++) {
            let possibleTripletToAdd = streetsAndTripletsThatCanBeAdded[i];

            // test if we can add the street/triplet on this branch
            let tripletIsOption = !possibleTripletToAdd.containsSameTileAs(pair);
            for (let i2 = 0; i2 < streetsAndTripletsThisBranch.length && tripletIsOption; i2++) {
                // does it overlap with other suites in this branch?
                tripletIsOption = !possibleTripletToAdd.containsSameTileAs(streetsAndTripletsThisBranch[i2]);
            }
            if (!tripletIsOption) {
                // overlap - this is not an option
                continue;
            }

            // the branch as it looks with this street/triplet added
            let newlyAddedTriplets: Array<Street|Triplet> =
                [possibleTripletToAdd].concat(streetsAndTripletsThisBranch);

            // the other streets/triplets which are not added here
            let otherStreetsAndTriplets: Array<Street|Triplet> = [];
            for (let i3 = i + 1; i3 < streetsAndTripletsThatCanBeAdded.length; i3++) {
                if (i3 !== i) {
                    otherStreetsAndTriplets.push(streetsAndTripletsThatCanBeAdded[i3]);
                }
            }

            // branch here
            results = results.concat(StandardSuiteSetDetection.getPermutations(
                newlyAddedTriplets,
                otherStreetsAndTriplets,
                pair,
                amountOfStreetsAndTripletsRequired - 1
            ));
        }

        return results;
    }

    /**
     * Filters the street suites out of an array of suites
     * @param triplets
     */
    private static filterSuitesForStreets(triplets: Array<Street|Triplet>): Array<Street> {
        let result: Array<Street> = [];

        for (let i = 0; i < triplets.length ; i++) {
            if (triplets[i].getSuiteType() === 'chi') {
                result.push(<Street>triplets[i]);
            }
        }

        return result;
    }

    /**
     * Filters the triplet suites out of an array of suites
     * @param triplets
     */
    private static filterSuitesForTriplets(triplets: Array<Street|Triplet>): Array<Triplet> {
        let result: Array<Triplet> = [];

        for (let i = 0; i < triplets.length ; i++) {
            if (triplets[i].getSuiteType() === 'pon') {
                result.push(<Triplet>triplets[i]);
            }
        }

        return result;
    }

    /**
     * Returns all possible standardSuiteSet that can be created with the tiles in a player hands
     * @param hand
     */
    public static standardSuiteSetsOfHand(hand: Hand): Array<StandardSuiteSetDetection> {

        // all tiles who are not yet locked into a suite. Out of this we have to create
        // extra streets and triplets to fill up
        let closedTiles = hand.closedTiles.concat([hand.extraTile]);

        // calculate all possible pairs, streets and triplet suites possible from
        // the (closed) tiles who are not yet locked into a suite
        let possiblePairs = StandardSuiteSetDetection.getPossiblePairs(closedTiles);
        let possibleClosedStreets = StandardSuiteSetDetection.getPossibleStreets(closedTiles);
        let possibleClosedTriplets = StandardSuiteSetDetection.getPossibleClosedTriplets(closedTiles);

        let possibleClosedTripletsOrStreets: Array<Triplet|Street> = [];
        possibleClosedTripletsOrStreets = possibleClosedTripletsOrStreets.concat(possibleClosedStreets);
        possibleClosedTripletsOrStreets = possibleClosedTripletsOrStreets.concat(possibleClosedTriplets);

        // the other suites. These are locked and have to be included
        let openTriplets: Array<Triplet> = hand.openTriplets;
        let openQuads: Array<Quad> = hand.openQuads;
        let openStreets: Array<Street> = hand.openStreets;
        let closedQuads: Array<Quad> = hand.closedQuads;

        // how many streets and/or triplets we need to build out of the closed tiles
        let numberOfStreetsOrTripletsFromClosedTiles: number =
            4
            - closedQuads.length
            - hand.openTriplets.length
            - hand.openQuads.length
            - hand.openStreets.length;

        // how many closed tiles we need to build the pair and street/triplets we require
        let numberOfClosedTilesRequired: number = numberOfStreetsOrTripletsFromClosedTiles * 3 + 2;

        if (closedTiles.length !== numberOfClosedTilesRequired) {
            // if this does not match the actual number of closed tiles, this won't work
            return [];
        }

        // standardSuiteSet contain exactly one pair, so lets try all possible
        // pairs and see if we can attach the rest to form a full suite set around the pair
        let possibleNormalHands: Array<StandardSuiteSetDetection> = [];
        for (let i = 0; i < possiblePairs.length; i++) {
            let pair = possiblePairs[i];

            // get a list of all possible suiteSets
            let permutations: Array<Array<Street|Triplet>> = StandardSuiteSetDetection.getPermutations(
                [],
                possibleClosedTripletsOrStreets,
                pair,
                numberOfStreetsOrTripletsFromClosedTiles
            );

            for (let permutation of permutations) {
                let closedChis: Array<Street> =
                    StandardSuiteSetDetection.filterSuitesForStreets(permutation);
                let closedPons: Array<Triplet> =
                    StandardSuiteSetDetection.filterSuitesForTriplets(permutation);

                possibleNormalHands.push(new StandardSuiteSet(
                    pair,
                    openStreets.concat(closedChis),
                    openTriplets.concat(closedPons),
                    openQuads.concat(closedQuads),
                ));
            }
        }

        return possibleNormalHands;
    }
}