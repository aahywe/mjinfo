import Hand from "@/lib/mahjong/model/Hand";
import Tile from "@/lib/mahjong/model/Tile";

import Pair from "@/lib/mahjong/model/suites/Pair";

import SevenPairsSuiteSet from "@/lib/mahjong/model/suiteSet/SevenPairsSuiteSet";

export default class SevenPairsHandSuiteSetDetection {
    public static tryBuildSevenPairsHand(hand: Hand): SevenPairsHandSuiteSetDetection | null {
        if (hand.closedQuads.length > 0 || hand.openTriplets.length > 0 || hand.openQuads.length > 0) {
            return null;
        }

        let allTiles: Array<Tile> = [hand.extraTile].concat(hand.closedTiles);
        if (allTiles.length !== 14) {
            // TODO: This should not happen
        }
        let tilePutIntoPair: Array<boolean> =
            [false, false, false, false, false, false, false, false, false, false, false, false, false, false];
        let foundPairs: Array<Pair> = [];

        for (let i = 0; i < allTiles.length; i++) {
            for (let i2 = 0; i2 < allTiles.length; i2++) {
                if (
                    i != i2 &&
                    !tilePutIntoPair[i] &&
                    !tilePutIntoPair[i2] &&
                    allTiles[i].tileColor === allTiles[i2].tileColor &&
                    allTiles[i].tileNumber === allTiles[i2].tileNumber
                ) {
                    foundPairs.push(new Pair(allTiles[i], allTiles[i2]));
                    tilePutIntoPair[i] = true;
                    tilePutIntoPair[i2] = true;
                }
            }
        }

        if (foundPairs.length === 7) {
            return new SevenPairsSuiteSet(foundPairs);
        }

        return null;
    }
}