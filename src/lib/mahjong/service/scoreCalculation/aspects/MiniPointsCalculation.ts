import SuiteSet from "@/lib/mahjong/model/SuiteSet";
import WinningState from "@/lib/mahjong/model/WinningState";
import Tile from "@/lib/mahjong/model/Tile";

import MiniPoints from "@/lib/mahjong/model/scoring/MiniPoints";
import SevenPairsMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/SevenPairsMiniPoints";
import ClosedHandAndCalledTile from "@/lib/mahjong/model/scoring/miniPoints/ClosedHandAndCalledTile";
import OpenHand from "@/lib/mahjong/model/scoring/miniPoints/OpenHand";
import TripletMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/TripletMiniPoints";
import QuadMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/QuadMiniPoints";
import SelfDrawnMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/SelfDrawnMiniPoints";
import SingleTileWaitMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/SingleTileWaitMiniPoints";
import DragonPairMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/DragonPairMiniPoints";
import SeatWindPairMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/SeatWindPairMiniPoints";
import RoundWindPairMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/RoundWindPairMiniPoints";
import OpenPinfuMiniPoints from "@/lib/mahjong/model/scoring/miniPoints/OpenPinfuMiniPoints";

import Triplet from "@/lib/mahjong/model/suites/Triplet";
import Quad from "@/lib/mahjong/model/suites/Quad";

export default class MiniPointsCalculation {

    /**
     * Returns the mini points awarded because the player has these triplets
     * @param triplets
     */
    private static getTripletMiniPoints(triplets: Array<Triplet>): Array<MiniPoints> {
        let miniPoints: Array<MiniPoints> = [];

        for (let triplet of triplets) {
            miniPoints.push(
                new TripletMiniPoints(
                    triplet.firstTile.tileType !== 'simple',
                    !triplet.getIsOpen()
                )
            );
        }

        return miniPoints;
    }

    /**
     * Returns the mini points awarded because the player has these quads
     * @param quads
     */
    private static getQuadMiniPoints(quads: Array<Quad>): Array<MiniPoints> {
        let miniPoints: Array<MiniPoints> = [];

        for (let quad of quads) {
            miniPoints.push(
                new QuadMiniPoints(
                    quad.firstTile.tileType !== 'simple',
                    !quad.getIsOpen()
                )
            );
        }

        return miniPoints;
    }

    /**
     * Returns true if at least one suite in the hand is open
     * @param handConfiguration
     */
    private static hasOpenSuite(handConfiguration: SuiteSet): boolean {
        for (let quad of handConfiguration.getQuads()) {
            if (quad.getIsOpen()) {
                return true;
            }
        }

        for (let triplet of handConfiguration.getTriplets()) {
            if (triplet.getIsOpen()) {
                return true;
            }
        }

        for (let street of handConfiguration.getStreets()) {
            if (street.getIsOpen()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the miniPoints for the suiteSet under the winningState with winningTile.
     * @param handConfiguration
     * @param winningState
     * @param winningTiles
     */
    public static getMiniPointsForHandConfiguration(
        handConfiguration: SuiteSet,
        winningState: WinningState,
        winningTiles: Array<Tile> | null
    ): Array<MiniPoints> {
        let miniPoints: Array<MiniPoints> = [];

        // player gets one of these as base points
        switch (true) {
            // when the hand is a special seven pairs hand
            case handConfiguration.getType() === 'HAND_CONFIGURATION_SEVEN_PAIRS':
                miniPoints.push(new SevenPairsMiniPoints());
                // no further points awarded
                return miniPoints;

            // when the hand is closed and the player robbed the final tile from the other player
            case !MiniPointsCalculation.hasOpenSuite(handConfiguration)
                && (
                    winningState.getFinalTileDrawnState() === 'otherDrawn'
                    || winningState.getFinalTileDrawnState() === 'quadRobbedDrawn'
                ):
                miniPoints.push(new ClosedHandAndCalledTile());
                break;

            // if not one of the above
            default:
                miniPoints.push(new OpenHand());
        }

        // mini points for waiting while only one tile could finish the hand
        if (winningTiles !== null && winningTiles.length === 1) {
            miniPoints.push(new SingleTileWaitMiniPoints());
        }

        // mini points for each triplet and quad suite
        miniPoints = miniPoints.concat(MiniPointsCalculation.getTripletMiniPoints(handConfiguration.getTriplets()));
        miniPoints = miniPoints.concat(MiniPointsCalculation.getQuadMiniPoints(handConfiguration.getQuads()));

        // mini points for certain types of pairs
        for (let pair of handConfiguration.getPairs()) {
            let tile = pair.firstTile;

            // a pair of dragons
            if (tile.tileColor === 'dragon') {
                miniPoints.push(new DragonPairMiniPoints());
            }

            // a pair of seat wind
            if (tile.tileNumber === winningState.getSeatWind()) {
                miniPoints.push(new SeatWindPairMiniPoints());
            }

            // a pair of round wind
            if (tile.tileNumber === winningState.getRoundWind()) {
                miniPoints.push(new RoundWindPairMiniPoints());
            }
        }

        // if no other mini points awarded but the tile is self drawn, the player gets 2 extra points
        if (
            miniPoints.length !== 1
            && (winningState.getFinalTileDrawnState() === 'selfDrawn' || winningState.getFinalTileDrawnState() === 'quadSupplementTileDrawn')
        ) {
            miniPoints.push(new SelfDrawnMiniPoints());
        }

        // if (still) no other mini points but the hand is open, the player gets 2 extra points
        if (miniPoints.length === 1 && MiniPointsCalculation.hasOpenSuite(handConfiguration)) {
            miniPoints.push(new OpenPinfuMiniPoints());
        }

        return miniPoints;
    }
}