import SuiteSet from "@/lib/mahjong/model/SuiteSet";
import SevenPairsSuiteSet from "@/lib/mahjong/model/suiteSet/SevenPairsSuiteSet";

import WinningPoints from "../../../model/scoring/WinningPoints";

import Tile, {TColor, TColorNonHonor, TNumberWindHonor, TTileType} from "@/lib/mahjong/model/Tile";
import WinningState from "@/lib/mahjong/model/WinningState";
import Suite from "@/lib/mahjong/model/Suite";

import Street from "@/lib/mahjong/model/suites/Street";

import AkaiDora from "@/lib/mahjong/model/scoring/winningPoints/AkaiDora";
import Dora from "@/lib/mahjong/model/scoring/winningPoints/Dora";
import UraDora from "@/lib/mahjong/model/scoring/winningPoints/UraDora";
import SevenPairs from "@/lib/mahjong/model/scoring/winningPoints/SevenPairs";
import Richii from "@/lib/mahjong/model/scoring/winningPoints/Richii";
import HandCompletleyClosed from "@/lib/mahjong/model/scoring/winningPoints/HandCompletleyClosed";
import Ippatsu from "@/lib/mahjong/model/scoring/winningPoints/FirstRoundAfterRichiiDeclaration";
import HalfFlush from "@/lib/mahjong/model/scoring/winningPoints/HalfFlush";
import FullFlush from "@/lib/mahjong/model/scoring/winningPoints/FullFlush";
import CompleteStraight from "@/lib/mahjong/model/scoring/winningPoints/CompleteStraight";
import DuplicateStreets from "@/lib/mahjong/model/scoring/winningPoints/DuplicateStreets";
import AllSimples from "@/lib/mahjong/model/scoring/winningPoints/AllSimples";
import TerminalInEachSuit from "@/lib/mahjong/model/scoring/winningPoints/TerminalInEachSuit";
import TerminalOrHonorInEachSet from "@/lib/mahjong/model/scoring/winningPoints/TerminalOrHonorInEachSet";
import LastTileSelfDrawn from "@/lib/mahjong/model/scoring/winningPoints/LastTileSelfDrawn";
import LastTileOtherDrawn from "@/lib/mahjong/model/scoring/winningPoints/LastTileOtherDrawn";
import DeadWallDraw from "@/lib/mahjong/model/scoring/winningPoints/DeadWallDraw";
import QuadRobbed from "@/lib/mahjong/model/scoring/winningPoints/QuadRobbed";
import DoubleDuplicateStreets from "@/lib/mahjong/model/scoring/winningPoints/DoubleDuplicateStreets";
import ParallelStreets from "@/lib/mahjong/model/scoring/winningPoints/ParallelStreets";
import AllTripletsOrQuads from "@/lib/mahjong/model/scoring/winningPoints/AllTripletsOrQuads";
import ThreeCloseedQuadsOrTriplets from "@/lib/mahjong/model/scoring/winningPoints/ThreeQuadsOrTriplets";
import ThreeQuads from "@/lib/mahjong/model/scoring/winningPoints/ThreeQuads";
import ThreeParallelTripletsOrKans from "@/lib/mahjong/model/scoring/winningPoints/ThreeParallelTripletsOrKans";
import DragonTriplet from "@/lib/mahjong/model/scoring/winningPoints/DragonTriplet";
import RoundWindTriplet from "@/lib/mahjong/model/scoring/winningPoints/RoundWindTriplet";
import SeatWindTriplet from "@/lib/mahjong/model/scoring/winningPoints/SeatWindTriplet";
import LittleThreeDragons from "@/lib/mahjong/model/scoring/winningPoints/LittleThreeDragons";
import DoubleRichii from "@/lib/mahjong/model/scoring/winningPoints/DoubleRichii";
import NoMiniPoints from "@/lib/mahjong/model/scoring/winningPoints/NoMiniPoints";

import MiniPoints from "@/lib/mahjong/model/scoring/MiniPoints";

export default class WinningPointsCalculation {

    /**
     * Returns true when [luckyTile] rewards [tile] lucky points
     * @param luckyTile
     * @param tile
     */
    private static tileIsLuckyTileFor(luckyTile: Tile, tile: Tile) {
        if (luckyTile.tileColor !== tile.tileColor) {
            return false;
        }

        switch (luckyTile.tileNumber) {
            case '1':
                return tile.tileNumber === '2';
            case '2':
                return tile.tileNumber === '3';
            case '3':
                return tile.tileNumber === '4';
            case '4':
                return tile.tileNumber === '5';
            case '5':
                return tile.tileNumber === '6';
            case '6':
                return tile.tileNumber === '7';
            case '7':
                return tile.tileNumber === '8';
            case '8':
                return tile.tileNumber === '9';
            case '9':
                return tile.tileNumber === '1';
            case 'red':
                return tile.tileNumber === 'white';
            case 'white':
                return tile.tileNumber === 'green';
            case 'green':
                return tile.tileNumber === 'red';
            case 'east':
                return tile.tileNumber === 'south';
            case 'south':
                return tile.tileNumber === 'west';
            case 'west':
                return tile.tileNumber === 'north';
            case 'north':
                return tile.tileNumber === 'east';
            default:
                throw new RangeError('WinningPointsCalculation.ts/tileIsLuckyTileFor: Switch trap');
        }
    }

    /**
     * Returns true if at least one suite in [suiteSet] is open
     * @param handConfiguration
     */
    private static hasOpenSuite(handConfiguration: SuiteSet): boolean {
        for (let quad of handConfiguration.getQuads()) {
            if (quad.getIsOpen()) {
                return true;
            }
        }

        for (let triplet of handConfiguration.getTriplets()) {
            if (triplet.getIsOpen()) {
                return true;
            }
        }

        for (let street of handConfiguration.getStreets()) {
            if (street.getIsOpen()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns all winning points based on under which conditions the player has won the round
     * @param winningState
     * @param handConfiguration
     */
    private static winningStateBasedWinningPoints(
        winningState: WinningState,
        handConfiguration: SuiteSet
    ): Array<WinningPoints> {
        let winningPoints: Array<WinningPoints> = [];

        switch (winningState.getRichiiState()) {
            case 'with':
                // player has won with richii rule
                winningPoints.push(new Richii());
                break;

            case 'withDouble':
                // player has won with double-richii rule
                winningPoints.push(new DoubleRichii());
                break;

            case 'with_immediate':
                // player has won with richii rule during the first turn after declaring
                winningPoints.push(new Richii());
                winningPoints.push(new Ippatsu());
                break;

            // player has won with double-richii rule during the first turn after declaring
            case 'withDouble_immediate':
                winningPoints.push(new DoubleRichii());
                winningPoints.push(new Ippatsu());
                break;
        }

        switch (winningState.getFinalTileDrawnState()) {
            case 'otherDrawn':
                if (winningState.getLastTile()) {
                    // player robbed the last tile dropped from another player to win the game
                    winningPoints.push(new LastTileOtherDrawn());
                }
                break;

            case 'selfDrawn':
                if (!this.hasOpenSuite(handConfiguration)) {
                    // player has a completely closed hand and drew the final piece himself
                    winningPoints.push(new HandCompletleyClosed());
                }
                if (winningState.getLastTile()) {
                    // player drew the last tile of the game to win it
                    winningPoints.push(new LastTileSelfDrawn());
                }
                break;

            case 'quadSupplementTileDrawn':
                // player won the game with the additional tile drawn after forming a quad
                winningPoints.push(new DeadWallDraw());
                if (!this.hasOpenSuite(handConfiguration)) {
                    // player has a completely closed hand and drew the final piece himself
                    winningPoints.push(new HandCompletleyClosed());
                }
                if (winningState.getLastTile()) {
                    // player drew the last tile of the game to win it
                    winningPoints.push(new LastTileSelfDrawn());
                }
                break;

            case 'quadRobbedDrawn':
                // player won the game by robbing the tile while another player formed a quad
                winningPoints.push(new QuadRobbed());
                if (winningState.getLastTile()) {
                    // player robbed the last tile dropped from another player to win the game
                    winningPoints.push(new LastTileOtherDrawn());
                }
                break;

            default:
                throw new Error('YakuPointsCalculation/winningStateBasedPoints: Invalid switch value');
        }

        return winningPoints;
    }

    /**
     * Returns the winningPoints based on lucky tiles
     * @param winningState
     * @param handConfiguration
     * @param doraTiles
     * @param uraDoraTiles
     */
    private static luckTileBasedWinningPoints(
        winningState: WinningState,
        handConfiguration: SuiteSet,
        doraTiles: Array<Tile>,
        uraDoraTiles: Array<Tile>
    ): Array<WinningPoints> {
        let winningPoints: Array<WinningPoints> = [];

        for (let tile of handConfiguration.getAllTiles()) {
            if (tile.isRedFive) {
                // player gets points for having a red-5 tile in his hand
                winningPoints.push(new AkaiDora(tile));
            }

            for (let doraTile of doraTiles) {
                if (WinningPointsCalculation.tileIsLuckyTileFor(doraTile, tile)) {
                    // player gets points for a lucky tile rewarding a tile in his hand
                    winningPoints.push(new Dora(doraTile, tile));
                }
            }

            // register bonus for richii lucky tile
            if (winningState.getRichiiState() !== 'without') {
                for (let uraDoraTile of uraDoraTiles) {
                    if (WinningPointsCalculation.tileIsLuckyTileFor(uraDoraTile, tile)) {
                        // player gets points for a lower lucky tile rewarding a tile in his hand after declaring richii
                        winningPoints.push(new UraDora(uraDoraTile, tile));
                    }
                }
            }
        }

        return winningPoints;
    }

    /**
     * True if at least one tile in the hand is of that color
     * @param handConfiguration
     */
    private static colorPresent(
        handConfiguration: SuiteSet
    ): {[color in TColor]: boolean} {
        let found: {[color in TColor]: boolean} = {
            'man': false,
            'pin': false,
            'bamboo': false,
            'wind': false,
            'dragon': false
        };

        for (let tile of handConfiguration.getAllTiles()) {
            found[tile.tileColor] = true;
        }

        return found;
    }

    /**
     * Counts the number of tiles in the hand which are non-honor (simple or terminal tile)
     * @param handConfiguration
     */
    private static nonHonorColorsCount(
        handConfiguration: SuiteSet
    ): number {
        let colorsPresent = WinningPointsCalculation.colorPresent(handConfiguration);

        return (colorsPresent.man ? 1 : 0)
            + (colorsPresent.pin ? 1 : 0)
            + (colorsPresent.bamboo ? 1 : 0);
    }

    /**
     * Returns the winningPoints based on flushes
     * @param handConfiguration
     */
    private static flushBasedWinningPoints(
        handConfiguration: SuiteSet
    ): Array<WinningPoints> {
        let colorsPresent = WinningPointsCalculation.colorPresent(handConfiguration);

        let nonHonorColorsCount = (colorsPresent.man ? 1 : 0)
            + (colorsPresent.pin ? 1 : 0)
            + (colorsPresent.bamboo ? 1 : 0);

        let honorTilesPresent = colorsPresent.wind || colorsPresent.dragon;

        if (nonHonorColorsCount === 1 && honorTilesPresent) {
            // player has a hand with only one color, excluding honor (dragon /wind) tiles
            return [new HalfFlush(handConfiguration.isClosed())]
        } else if (nonHonorColorsCount === 1) {
            // player has a hand with only one color
            return [new FullFlush(handConfiguration.isClosed())]
        }

        return [];
    }

    /**
     * Returns true if the streets contain streets 1-2-3, 4-5-6, 7-8-9 in the same color
     * @param streets
     */
    private static containsBigStreet(streets: Array<Street>): boolean {
        let detectedStreets: {[color in TColorNonHonor]: {oneTwoThree: boolean, fourFiveSix: boolean, sevenEightNine: boolean}} =
            {
                'pin': {'oneTwoThree': false, 'fourFiveSix': false, 'sevenEightNine': false},
                'man': {'oneTwoThree': false, 'fourFiveSix': false, 'sevenEightNine': false},
                'bamboo': {'oneTwoThree': false, 'fourFiveSix': false, 'sevenEightNine': false},
            };
        
        for (let street of streets) {
            if (
                street.lowestTile.tileNumber === '1'
                && street.midTile.tileNumber === '2'
                && street.upperTile.tileNumber === '3'
            ) {
                detectedStreets[street.lowestTile.tileColor as TColorNonHonor].oneTwoThree = true;
            } else if (
                street.lowestTile.tileNumber === '4'
                && street.midTile.tileNumber === '5'
                && street.upperTile.tileNumber === '6'
            ) {
                detectedStreets[street.lowestTile.tileColor as TColorNonHonor].fourFiveSix = true;
            } else if (
                street.lowestTile.tileNumber === '7'
                && street.midTile.tileNumber === '8'
                && street.upperTile.tileNumber === '9'
            ) {
                detectedStreets[street.lowestTile.tileColor as TColorNonHonor].sevenEightNine = true;
            }
        }
            
        return (detectedStreets.pin.oneTwoThree && detectedStreets.pin.fourFiveSix && detectedStreets.pin.sevenEightNine)
            || (detectedStreets.man.oneTwoThree && detectedStreets.man.fourFiveSix && detectedStreets.man.sevenEightNine)
            || (detectedStreets.bamboo.oneTwoThree && detectedStreets.bamboo.fourFiveSix && detectedStreets.bamboo.sevenEightNine);
    }

    /**
     * Counts the number of times streets have the same color and numbers
     * @param streets
     */
    private static duplicateStreetsCount(streets: Array<Street>): number {
        let duplicates = 0;

        for (let i = 0; i < streets.length; i++) {
            let street = streets[i];
            for (let i2 = i + 1; i2 < streets.length; i2++) {
                let street2 = streets[i2];

                if (
                    street.lowestTile.tileNumber === street2.lowestTile.tileNumber
                    && street.midTile.tileNumber === street2.midTile.tileNumber
                    && street.upperTile.tileNumber === street2.upperTile.tileNumber
                    && street.lowestTile.tileColor === street2.lowestTile.tileColor
                ) {
                    duplicates++;
                }
            }
        }

        return duplicates;
    }

    /**
     * Returns true if the streets contain three streets with the same number but different colors
     * @param streets
     */
    private static hasParallelStreets(streets: Array<Street>): boolean {
        for (let i = 0; i < streets.length; i++) {
            let street = streets[i];
            
            for (let i2 = i + 1; i2 < streets.length; i2++) {
                let street2 = streets[i2];
                
                for (let i3 = i2 + 1; i3 < streets.length; i3++) {
                    let street3 = streets[i3];

                    if (
                        street.lowestTile.tileNumber === street2.lowestTile.tileNumber
                        && street.lowestTile.tileNumber === street3.lowestTile.tileNumber
                        && street.midTile.tileNumber === street2.midTile.tileNumber
                        && street.midTile.tileNumber === street3.midTile.tileNumber
                        && street.upperTile.tileNumber === street2.upperTile.tileNumber
                        && street.upperTile.tileNumber === street3.upperTile.tileNumber
                        && street.lowestTile.tileColor !== street2.lowestTile.tileColor
                        && street.lowestTile.tileColor !== street3.lowestTile.tileColor
                        && street2.lowestTile.tileColor !== street3.lowestTile.tileColor
                    ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Returns winning points based on streets
     * @param handConfiguration
     */
    private static streetBasedWinningPoints(
        handConfiguration: SuiteSet,
    ): Array<WinningPoints> {
        let winningPoints: Array<WinningPoints> = [];

        if (WinningPointsCalculation.containsBigStreet(handConfiguration.getStreets())) {
            // player has streets of 1-2-3, 4-5-6, 7-8-9 in same color
            winningPoints.push(new CompleteStraight(handConfiguration.isClosed()));
        }

        if (handConfiguration.isClosed()) {
            switch (WinningPointsCalculation.duplicateStreetsCount(handConfiguration.getStreets())) {
                case 1:
                    // player has two streets with same numbers and color
                    winningPoints.push(new DuplicateStreets());
                    break;
                case 2:
                    // player has two times two streets with same numbers and color
                    winningPoints.push(new DoubleDuplicateStreets());
                    break;
            }
        }

        if (WinningPointsCalculation.hasParallelStreets(handConfiguration.getStreets())) {
            // player has three streets with same number, but different colors
            winningPoints.push(new ParallelStreets(handConfiguration.isClosed()));
        }

        return winningPoints;
    }

    /**
     * Counts the simple, terminal and honor tiles in a hand
     * @param handConfiguration
     */
    private static tileTypesCount(
        handConfiguration: SuiteSet
    ): {[tileType in TTileType]: number} {
        let tileTypeCount: {[tileType in TTileType]: number} = {
            'simple': 0,
            'terminal': 0,
            'honor': 0
        };

        for (let tile of handConfiguration.getAllTiles()) {
            tileTypeCount[tile.tileType]++;
        }

        return tileTypeCount;
    }

    /**
     * Check if each suite has at least one simple, terminal or honor tile
     * @param handConfiguration
     */
    private static tileTypeInEachSuiteAtLeastOnce(
        handConfiguration: SuiteSet
    ): {[tileType in TTileType]: boolean} {
        let allSuites: Array<Suite> = (handConfiguration.getStreets() as Array<Suite>)
            .concat(handConfiguration.getTriplets() as Array<Suite>)
            .concat(handConfiguration.getQuads() as Array<Suite>);

        let tileTypeInEachSuite: {[tileType in TTileType]: boolean} = {
            'simple': true,
            'terminal': true,
            'honor': true
        };

        for (let suite of allSuites) {
            let found: {[tileType in TTileType]: boolean} = {
                'simple': false,
                'terminal': false,
                'honor': false
            };

            for (let tile of suite.getTiles()) {
                found[tile.tileType] = true;
            }

            tileTypeInEachSuite.simple = tileTypeInEachSuite.simple && found.simple;
            tileTypeInEachSuite.terminal = tileTypeInEachSuite.terminal && found.terminal;
            tileTypeInEachSuite.honor = tileTypeInEachSuite.honor && found.honor;
        }

        return tileTypeInEachSuite;
    }

    /**
     * Returns winning points based on the tile types in the suites
     * @param handConfiguration
     */
    private static tileTypeBasedWinningPoints(
        handConfiguration: SuiteSet
    ): Array<WinningPoints> {
        let winningPoints: Array<WinningPoints> = [];

        let tileTypeCount = WinningPointsCalculation.tileTypesCount(handConfiguration);
        let tileTypeInEachSuite = WinningPointsCalculation.tileTypeInEachSuiteAtLeastOnce(handConfiguration);

        if (tileTypeCount.terminal === 0 && tileTypeCount.honor === 0) {
            // players hand has only simple tiles
            return [new AllSimples()];
        } else if (tileTypeInEachSuite.terminal && tileTypeInEachSuite.honor) {
            // player has at least one terminal or honor in each suite
            return [new TerminalOrHonorInEachSet(handConfiguration.isClosed())];
        } else if (tileTypeInEachSuite.terminal) {
            // player has at least one terminal in each suite
            return [new TerminalInEachSuit(handConfiguration.isClosed())];
        }

        return [];
    }

    /**
     * Returns winningPoints base on the hand type
     * @param handConfiguration
     */
    private static handConfigurationBasedWinningPoints(
        handConfiguration: SuiteSet
    ): Array<WinningPoints> {
        if (handConfiguration.getType() === 'HAND_CONFIGURATION_SEVEN_PAIRS') {
            // player has a seven pair hand
            return [new SevenPairs(handConfiguration as SevenPairsSuiteSet)];
        }

        return [];
    }

    /**
     * Counts the closed triplets in a hand
     * @param handConfiguration
     */
    private static closedTripletCount(
        handConfiguration: SuiteSet
    ): number {
        let closedTriplets: number = 0;

        for (let suite of handConfiguration.getTriplets()) {
            if (!suite.getIsOpen()) {
                closedTriplets++;
            }
        }

        return closedTriplets;
    }

    /**
     * Counts the closed quads in a hand
     * @param handConfiguration
     */
    private static closedQuadCount(
        handConfiguration: SuiteSet
    ): number {
        let closedQuads: number = 0;

        for (let suite of handConfiguration.getQuads()) {
            if (!suite.getIsOpen()) {
                closedQuads++;
            }
        }

        return closedQuads;
    }

    /**
     * Returns true if the hand as three triplets/quads with the same number but different colors
     * @param handConfiguration
     */
    private static threeParallelTripletsOrKans(
        handConfiguration: SuiteSet
    ): boolean {
        let suites: Array<Suite> =
            (handConfiguration.getTriplets() as Array<Suite>).concat(handConfiguration.getQuads() as Array<Suite>);

        for (let i = 0;  i < suites.length; i++) {
            let suite = suites[i];

            for (let i2 = i + 1; i2 < suites.length; i2++) {
                let suite2 = suites[i2];

                for (let i3 = i2 + 1; i3 <suites.length; i3++) {
                    let suite3 = suites[i3];

                    if (
                        suite.getTiles()[0].tileNumber === suite2.getTiles()[0].tileNumber
                        && suite.getTiles()[0].tileNumber === suite3.getTiles()[0].tileNumber
                        && suite.getTiles()[0].tileColor !== suite2.getTiles()[0].tileColor
                        && suite.getTiles()[0].tileColor !== suite3.getTiles()[0].tileColor
                        && suite2.getTiles()[0].tileColor !== suite3.getTiles()[0].tileColor
                    ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Counts the amount of dragon triplet or quads
     * @param handConfiguration
     */
    private static dragonTripletOrQuads(
        handConfiguration: SuiteSet
    ): number {
        let dragonTripletOrQuads: number = 0;

        let suites: Array<Suite> =
            (handConfiguration.getTriplets() as Array<Suite>).concat(handConfiguration.getQuads() as Array<Suite>);

        for (let suite of suites) {
            if (suite.getTiles()[0].tileColor === 'dragon') {
                dragonTripletOrQuads++;
            }
        }

        return dragonTripletOrQuads;
    }

    /**
     * Returns true if the hand has a dragon pair
     * @param handConfiguration
     */
    private static hasDragonPair(
        handConfiguration: SuiteSet
    ): boolean {
        for (let pair of handConfiguration.getPairs()) {
            if (pair.firstTile.tileColor === 'dragon') {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns true if there is a triplet or quad of [wind]
     * @param handConfiguration
     * @param wind
     */
    private static containsWindAsTripletOrQuad(
        handConfiguration: SuiteSet,
        wind: TNumberWindHonor
    ): boolean {
        let suites: Array<Suite> =
            (handConfiguration.getTriplets() as Array<Suite>).concat(handConfiguration.getQuads() as Array<Suite>);

        for (let suite of suites) {
            if (suite.getTiles()[0].tileColor === 'wind' && suite.getTiles()[0].tileNumber === wind) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns winningPoints based on triplets or quads
     * @param handConfiguration
     * @param winningState
     */
    private static tripletOrQuadBasedWinningPoints(
        handConfiguration: SuiteSet,
        winningState: WinningState
    ): Array<WinningPoints> {
        let winningPoints: Array<WinningPoints> = [];

        let closedTriplets = WinningPointsCalculation.closedTripletCount(handConfiguration);
        let closedQuads = WinningPointsCalculation.closedQuadCount(handConfiguration);

        if (handConfiguration.getStreets().length === 0 && handConfiguration.getPairs().length === 1) {
            // player has a hand with only triplets and quads plus one pair
            winningPoints.push(new AllTripletsOrQuads(handConfiguration.isClosed()));
        }

        if (closedQuads + closedTriplets === 3) {
            // player has three closed triplets
            winningPoints.push(new ThreeCloseedQuadsOrTriplets());
        }

        if (handConfiguration.getQuads().length === 3) {
            // player has three quads
            winningPoints.push(new ThreeQuads());
        }

        if (WinningPointsCalculation.threeParallelTripletsOrKans(handConfiguration)) {
            // player has three triplets/quads with same number but different colors
            winningPoints.push(new ThreeParallelTripletsOrKans());
        }

        if (WinningPointsCalculation.containsWindAsTripletOrQuad(handConfiguration, winningState.getRoundWind())) {
            // player has the round wind as a triplet
            winningPoints.push(new RoundWindTriplet());
        }

        if (WinningPointsCalculation.containsWindAsTripletOrQuad(handConfiguration, winningState.getSeatWind())) {
            // player has his seat wind as a triplet
            winningPoints.push(new SeatWindTriplet());
        }

        switch(WinningPointsCalculation.dragonTripletOrQuads(handConfiguration)) {
            case 1:
                // player has a dragon triplet
                winningPoints.push(new DragonTriplet());
                break;

            case 2:
                // player has two dragon triplet
                winningPoints.push(new DragonTriplet());
                winningPoints.push(new DragonTriplet());
                if (WinningPointsCalculation.hasDragonPair(handConfiguration)) {
                    // player has two dragon triplet plus the third dragon type as a pair
                    winningPoints.push(new LittleThreeDragons());
                }
        }

        return winningPoints;
    }

    /**
     * Returns winning points base on mini Points
     * @param winningState
     * @param miniPoints
     */
    private static miniPointsBasedWinningPoints(
        winningState: WinningState,
        miniPoints: Array<MiniPoints>
    ): Array<WinningPoints> {
        if (miniPoints.length === 1 && miniPoints[0].getScore() !== 25) {
            // player has only base mini points
            return [new NoMiniPoints()];
        }

        return [];
    }

    /**
     * Returns the winningPoints for [suiteSet], assuming he won with [miniPoints] in [winningState]
     * and [doraTiles] and [uraDoraTiles] are the lucky tiles
     * @param handConfiguration
     * @param doraTiles
     * @param uraDoraTiles
     * @param winningState
     * @param miniPoints
     */
    public static winningPointsCalculation(
        handConfiguration: SuiteSet,
        doraTiles: Array<Tile>,
        uraDoraTiles: Array<Tile>,
        winningState: WinningState,
        miniPoints: Array<MiniPoints>
    ): Array<WinningPoints> {
        return WinningPointsCalculation.miniPointsBasedWinningPoints(winningState, miniPoints)
            .concat(WinningPointsCalculation.winningStateBasedWinningPoints(winningState, handConfiguration))
            .concat(WinningPointsCalculation.luckTileBasedWinningPoints(winningState, handConfiguration, doraTiles, uraDoraTiles))
            .concat(WinningPointsCalculation.flushBasedWinningPoints(handConfiguration))
            .concat(WinningPointsCalculation.streetBasedWinningPoints(handConfiguration))
            .concat(WinningPointsCalculation.tileTypeBasedWinningPoints(handConfiguration))
            .concat(WinningPointsCalculation.handConfigurationBasedWinningPoints(handConfiguration))
            .concat(WinningPointsCalculation.tripletOrQuadBasedWinningPoints(handConfiguration, winningState));
    }
}