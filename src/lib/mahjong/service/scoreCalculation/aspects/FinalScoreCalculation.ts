/* eslint no-case-declarations: 0 */

import WinningPoints from "../../../model/scoring/WinningPoints";

import Yakuman from "@/lib/mahjong/model/scoring/Yakuman";

import MiniPoints from "@/lib/mahjong/model/scoring/MiniPoints";

const mangan: number = 8000;
const manganEast: number = 12000;
const manganPortion: number = 2000;
const manganPortionEast: number = 4000;

const haneman: number = 12000;
const hanemanEast: number = 18000;

const baiman: number = 18000;
const baimanEast: number = 24000;

const sanBaiman: number = 24000;
const sanBaimanEast: number = 36000;

const yakuman: number = 32000;
const yakumanEast: number = 48000;

const scoreNonEast: {[fuPoints: number]: {[yakuPoints: number]: number | null}} = {
    20: {1: null, 2: 400, 3: 700, 4: 1300},
    25: {1: null, 2: null, 3: 800, 4: 1600},
    30: {1: 300, 2: 500, 3: 1000, 4: 2000},
    40: {1: 400, 2: 700, 3: 1300, 4: manganPortion},
    50: {1: 400, 2: 800, 3: 1800, 4: manganPortion},
    60: {1: 500, 2: 1000, 3: 2000, 4: manganPortion},
    70: {1: 600, 2: 1200, 3: manganPortion, 4: manganPortion},
    80: {1: 700, 2: 1300, 3: manganPortion, 4: manganPortion},
    90: {1: 800, 2: 1500, 3: manganPortion, 4: manganPortion},
    100: {1: 800, 2: 1600, 3: manganPortion, 4: manganPortion},
    110: {1: 900, 2: 1800, 3: manganPortion, 4: manganPortion},
};

const scoreEast: {[fuPoints: number]: {[yakuPoints: number]: number | null}}  = {
    20: {1: null, 2: 700, 3: 1300, 4: 2600},
    25: {1: null, 2: null, 3: 1600, 4: 3200},
    30: {1: 500, 2: 1000, 3: 2000, 4: 3900},
    40: {1: 700, 2: 1300, 3: 2600, 4: manganPortionEast},
    50: {1: 800, 2: 1600, 3: 3200, 4: manganPortionEast},
    60: {1: 1000, 2: 2000, 3: 3900, 4: manganPortionEast},
    70: {1: 1200, 2: 2300, 3: manganPortionEast, 4: manganPortionEast},
    80: {1: 1300, 2: 2600, 3: manganPortionEast, 4: manganPortionEast},
    90: {1: 1500, 2: 2900, 3: manganPortionEast, 4: manganPortionEast},
    100: {1: 1600, 2: 3200, 3: manganPortionEast, 4: manganPortionEast},
    110: {1: 1800, 2: 3600, 3: manganPortionEast, 4: manganPortionEast},
};

export default class FinalScoreCalculation {
    private static getFuPointTablePoint(fuPoints: number): 20 | 25 | 30 | 40 | 50 | 60 | 70 | 80 | 90 | 100 | 110 {
        if (fuPoints < 20) {
            throw new RangeError('Too few fu points (' + fuPoints + ')')
        } else if (fuPoints === 20) {
            return 20;
        } else if (fuPoints === 25) {
            return 25;
        } else if (fuPoints <= 30) {
            return 30;
        } else if (fuPoints <= 40) {
            return 40;
        } else if (fuPoints <= 50) {
            return 50;
        } else if (fuPoints <= 60) {
            return 60;
        } else if (fuPoints <= 70) {
            return 70;
        } else if (fuPoints <= 80) {
            return 80;
        } else if (fuPoints <= 90) {
            return 90;
        } else if (fuPoints <= 100) {
            return 100;
        } else {
            return 110;
        }
    }

    public static totalScore(
        fuPoints: Array<MiniPoints>,
        yakuPoints: Array<WinningPoints>,
        yakumans: Array<Yakuman>,
        asEast: boolean,
        asTsumo: boolean
    ): number {
        let totalYakumans: number = 0;
        for (let yakuman of yakumans) {
            totalYakumans += yakuman.getYakumanPoints();
        }

        if (totalYakumans > 0) {
            return totalYakumans * (asEast ? yakumanEast : yakuman);
        }

        let totalYakuPoints: number = 0;
        for (let yakuPoint of yakuPoints) {
            totalYakuPoints += yakuPoint.getScore();
        }

        switch (totalYakuPoints) {
            case 1:
            case 2:
            case 3:
            case 4:
                let totalFuPoints: number = 0;
                for (let fuPoint of fuPoints) {
                    totalFuPoints += fuPoint.getScore();
                }

                let scoreEastPoint: number | null =
                    scoreEast[FinalScoreCalculation.getFuPointTablePoint(totalFuPoints)][totalYakuPoints];
                let scoreOtherPoint: number | null =
                    scoreNonEast[FinalScoreCalculation.getFuPointTablePoint(totalFuPoints)][totalYakuPoints];

                if (scoreEastPoint === null || scoreOtherPoint === null) {
                    throw new RangeError('FinalScoreCalculation.ts/totalScore: Invalid fu-yaku combination ' + totalFuPoints + ' with ' + totalYakuPoints);
                }

                if (asEast) {
                    return 3 * scoreEastPoint;
                } else {
                    return scoreEastPoint + 2 * scoreOtherPoint;
                }

            case 5:
                return asEast ? manganEast: mangan;
            case 6:
            case 7:
                return asEast ? hanemanEast: haneman;
            case 8:
            case 9:
            case 10:
                return asEast ? baimanEast: baiman;
            case 11:
            case 12:
                return asEast ? sanBaimanEast: sanBaiman;
            default: // >12
                return asEast ? yakumanEast: yakuman;
        }
    }
}