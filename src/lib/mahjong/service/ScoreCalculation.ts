import Hand from "@/lib/mahjong/model/Hand";
import Tile from "@/lib/mahjong/model/Tile";

import SuiteSet from "@/lib/mahjong/model/SuiteSet";
import SevenPairsHandSuiteSetDetection from "@/lib/mahjong/service/scoreCalculation/suiteSetDetection/SevenPairsHandSuiteSetDetection"
import StandardSuiteSetDetection from "@/lib/mahjong/service/scoreCalculation/suiteSetDetection/StandardSuiteSetDetection";
import MiniPoints from "@/lib/mahjong/model/scoring/MiniPoints";

import WinningPoints from "../model/scoring/WinningPoints";

import Yakuman from "@/lib/mahjong/model/scoring/Yakuman";

import MiniPointsCalculation from "@/lib/mahjong/service/scoreCalculation/aspects/MiniPointsCalculation";
import WinningPointsCalculation from "@/lib/mahjong/service/scoreCalculation/aspects/WinningPointsCalculation";
import YakumanCalculation from "@/lib/mahjong/service/scoreCalculation/aspects/YakumanCalculation";
import FinalScoreCalculation from "@/lib/mahjong/service/scoreCalculation/aspects/FinalScoreCalculation";
import Score from "@/lib/mahjong/model/Score";
import WinningState from "@/lib/mahjong/model/WinningState";

export default class ScoreCalculation {
    private static getAllConfigurations(hand: Hand): Array<SuiteSet> {
        let handConfigurations: Array<SuiteSet> =
            <Array<SuiteSet>>StandardSuiteSetDetection.standardSuiteSetsOfHand(hand);

        let possibleSevenPairsHand = SevenPairsHandSuiteSetDetection.tryBuildSevenPairsHand(hand);
        if (possibleSevenPairsHand !== null) {
            handConfigurations.push()
        }

        return handConfigurations;
    }

    public static calculateScore(
        hand: Hand,
        winningState: WinningState,
        doraTiles: Array<Tile>,
        uraDoraTiles: Array<Tile>,
        winningTiles: Array<Tile> | null,
    ): Score | null {
        let handConfigurations = ScoreCalculation.getAllConfigurations(hand);

        if (handConfigurations.length === 0) {
            return null;
        }

        let bestFinalScore: Score | null = null;
        for (let handConfigration of handConfigurations) {
            let miniPoints: Array<MiniPoints> =
                MiniPointsCalculation.getMiniPointsForHandConfiguration(
                    handConfigration,
                    winningState,
                    winningTiles
                );

            let yakuPoints: Array<WinningPoints> =
                WinningPointsCalculation.winningPointsCalculation(
                    handConfigration,
                    doraTiles,
                    uraDoraTiles,
                    winningState,
                    miniPoints
                );
            let yakumans: Array<Yakuman> =
                YakumanCalculation.getYakumanForHandConfiguration(handConfigration);

            if (yakuPoints.length === 0 && yakumans.length === 0) {
                return null;
            }

            let finalScore: number|null = FinalScoreCalculation.totalScore(
                miniPoints,
                yakuPoints,
                yakumans,
                winningState.getSeatWind() === 'east',
                winningState.getFinalTileDrawnState() === 'selfDrawn'
            );

            if (finalScore === null) {
                continue;
            }

            let thisScore: Score = new Score(
                handConfigration,
                miniPoints,
                yakuPoints,
            );

            switch (winningState.getFinalTileDrawnState()) {
                case 'selfDrawn':
                case 'quadSupplementTileDrawn':
                    if (
                        finalScore !== null
                        && (bestFinalScore === null || bestFinalScore.sumSelfDrawnAsAndAgainstOther > thisScore.sumSelfDrawnAsAndAgainstOther)) {
                        bestFinalScore = thisScore;
                    }
                    break;
                case 'otherDrawn':
                case 'quadRobbedDrawn':
                    if (
                        finalScore !== null
                        && (bestFinalScore === null || bestFinalScore.sumOtherDrawnAsNonEast > thisScore.sumOtherDrawnAsNonEast)) {
                        bestFinalScore = thisScore;
                    }
            }

        }

        return bestFinalScore;
    }
}