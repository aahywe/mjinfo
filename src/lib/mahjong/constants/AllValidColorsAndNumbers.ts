import {TColor, TNumber} from "../model/Tile";

export type TTilesList = Array<{color: TColor, numbers: Array<TNumber>}>;

// TODO: Refactor this away
export const allValidColorsAndNumbers: TTilesList = [
    {'color': 'pin', numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9']},
    {'color': 'man', numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9']},
    {'color': 'bamboo', numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9']},
    {'color': 'wind', numbers: ['east', 'south', 'west', 'north']},
    {'color': 'dragon', numbers: ['red', 'white', 'green']},
];
