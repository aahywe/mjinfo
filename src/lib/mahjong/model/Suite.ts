import Tile from "@/lib/mahjong/model/Tile";

export type TSuiteType = 'pair' | 'chi' | 'pon' | 'kan';

/**
 * Represents a suite (multiple tiles that a grouped)
 */
export default interface Suite {

    /**
     * True if the suite counts as an open suite for scoring
     */
    getIsOpen(): boolean;

    /**
     * An id to distinguish suite type
     */
    getSuiteType(): TSuiteType;

    /**
     * The tiles forming the suite
     */
    getTiles(): Array<Tile>;

    /**
     * Returns true if a tile in this suite is also in the other suite
     * @param otherSuite
     */
    containsSameTileAs(otherSuite: Suite): boolean;
}