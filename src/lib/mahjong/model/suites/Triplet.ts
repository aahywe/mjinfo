import Tile from "@/lib/mahjong/model/Tile";
import Suite, {TSuiteType} from "@/lib/mahjong/model/Suite";

export default class Triplet implements Suite {

    /**
     * Returns true if firstTile, secondTile and thirdTile together form a valid triplet suite
     * @param firstTile
     * @param secondTile
     * @param thirdTile
     */
    public static isValidPon(firstTile: Tile, secondTile: Tile, thirdTile: Tile): boolean {
        return firstTile.tileColor === secondTile.tileColor
            && firstTile.tileColor === thirdTile.tileColor
            && firstTile.tileNumber === secondTile.tileNumber
            && firstTile.tileNumber === thirdTile.tileNumber;
    }

    public readonly firstTile: Tile;
    public readonly secondTile: Tile;
    public readonly thirdTile: Tile;
    public readonly isOpen: boolean;

    /**
     * True if the suite counts as an open suite for scoring
     */
    public getIsOpen(): boolean {
        return this.isOpen;
    }

    /**
     * An id to distinguish suite type
     */
    public getSuiteType(): TSuiteType {
        return 'pon';
    }

    /**
     * The tiles forming the suite
     */
    public getTiles(): Array<Tile> {
        return [this.firstTile, this.secondTile, this.thirdTile];
    }

    /**
     * Returns true if a tile in this suite is also in the other suite
     * @param otherSuite
     */
    public containsSameTileAs(group: Suite): boolean {
        for (let tile of group.getTiles()) {
            if (tile === this.firstTile || tile === this.secondTile || tile === this.thirdTile) {
                return true;
            }
        }

        return false;
    }

    /**
     * Represents a triplet suite (three identical tiles)
     * @param firstTile
     * @param secondTile
     * @param thirdTile
     * @param isOpen
     */
    constructor(firstTile: Tile, secondTile: Tile, thirdTile: Tile, isOpen: boolean) {
        if (firstTile === secondTile || firstTile === thirdTile || secondTile === thirdTile) {
            throw new RangeError('Triplet.ts/constructor: Attempted to add same tile twice');
        }

        if (!Triplet.isValidPon(firstTile, secondTile, thirdTile)) {
            throw new RangeError('Triplet.ts/constructor: must be identical tiles');
        }

        this.firstTile = firstTile;
        this.secondTile = secondTile;
        this.thirdTile = thirdTile;
        this.isOpen = isOpen;
    }
}