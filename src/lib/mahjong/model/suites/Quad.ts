import Tile from "@/lib/mahjong/model/Tile";
import Suite, {TSuiteType} from "@/lib/mahjong/model/Suite";

export default class Quad implements Suite {

    /**
     * Returns true if the tiles together form a valid quad suite
     * @param firstTile
     * @param secondTile
     * @param thirdTile
     * @param fourthTile
     */
    public static isValidKan(firstTile: Tile, secondTile: Tile, thirdTile: Tile, fourthTile: Tile): boolean {
        return firstTile.tileColor === secondTile.tileColor
            && firstTile.tileColor === thirdTile.tileColor
            && firstTile.tileColor === fourthTile.tileColor
            && firstTile.tileNumber === secondTile.tileNumber
            && firstTile.tileNumber === thirdTile.tileNumber
            && firstTile.tileNumber === fourthTile.tileNumber;
    }

    public readonly firstTile: Tile;
    public readonly secondTile: Tile;
    public readonly thirdTile: Tile;
    public readonly fourthTile: Tile;
    public readonly isOpen: boolean;

    /**
     * True if the suite counts as an open suite for scoring
     */
    public getIsOpen(): boolean {
        return this.isOpen;
    }

    /**
     * An id to distinguish suite type
     */
    public getSuiteType(): TSuiteType {
        return 'kan';
    }

    /**
     * The tiles forming the suite
     */
    public getTiles(): Array<Tile> {
        return [this.firstTile, this.secondTile, this.thirdTile, this.fourthTile];
    }

    /**
     * Returns true if a tile in this suite is also in the other suite
     * @param otherSuite
     */
    public containsSameTileAs(otherTile: Suite): boolean {
        for (let tile of otherTile.getTiles()) {
            if (
                tile === this.firstTile ||
                tile === this.secondTile ||
                tile === this.thirdTile ||
                tile === this.fourthTile
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Represents a quad suite (four identical tiles)
     * @param firstTile
     * @param secondTile
     * @param thirdTile
     * @param fourthTile
     * @param isOpen
     */
    constructor(
        firstTile: Tile,
        secondTile: Tile,
        thirdTile: Tile,
        fourthTile: Tile,
        isOpen: boolean,
    ) {
        if (
            firstTile === secondTile || firstTile === thirdTile || firstTile === fourthTile ||
            secondTile === thirdTile || secondTile === fourthTile || thirdTile === fourthTile
        ) {
            throw new RangeError('Quad.ts/constructor: Attempted to add same tile twice');
        }

        if (!Quad.isValidKan(firstTile, secondTile, thirdTile, fourthTile)) {
            throw new RangeError('Quad.ts/constructor: Must be identical tiles');
        }

        this.firstTile = firstTile;
        this.secondTile = secondTile;
        this.thirdTile = thirdTile;
        this.fourthTile = fourthTile;
        this.isOpen = isOpen;
    }
}