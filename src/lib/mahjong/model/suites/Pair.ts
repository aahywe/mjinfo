import Tile from "@/lib/mahjong/model/Tile";
import Suite, {TSuiteType} from "@/lib/mahjong/model/Suite";

export default class Pair implements Suite {

    /**
     * Returns true if firstTile and secondTile together form a valid pair suite
     * @param firstTile
     * @param secondTile
     */
    public static isValidPair(firstTile: Tile, secondTile: Tile): boolean {
        return firstTile.tileColor === secondTile.tileColor
            && firstTile.tileNumber === secondTile.tileNumber;
    }

    public readonly firstTile: Tile;
    public readonly secondTile: Tile;

    /**
     * True if the suite counts as an open suite for scoring
     */
    public getIsOpen(): boolean {
        return false; // pairs are always closed
    }

    /**
     * An id to distinguish suite type
     */
    public getSuiteType(): TSuiteType {
        return 'pair';
    }

    /**
     * The tiles forming the suite
     */
    public getTiles(): Array<Tile> {
        return [this.firstTile, this.secondTile];
    }

    /**
     * Returns true if a tile in this suite is also in the other suite
     * @param otherSuite
     */
    public containsSameTileAs(otherSuite: Suite): boolean {
        for (let tile of otherSuite.getTiles()) {
            if (tile === this.firstTile || tile === this.secondTile) {
                return true;
            }
        }

        return false;
    }

    /**
     * Represents a pair suite (two identical tiles)
     * @param firstTile
     * @param secondTile
     */
    constructor(
        firstTile: Tile,
        secondTile: Tile,
    ) {
        if (firstTile === secondTile) {
            throw new RangeError('Pair.ts/constructor: Attempted to add same tile twice');
        }

        if (!Pair.isValidPair(firstTile, secondTile)) {
            throw new RangeError('Pair.ts/constructor: Must be identical tiles');
        }

        this.firstTile = firstTile;
        this.secondTile = secondTile;
    }
}