export type TYakumanType = 'YAKUMAN_SOMETHING'; // TODO

export default interface Yakuman {
    getYakumanPoints(): number;
    getType(): TYakumanType;
}