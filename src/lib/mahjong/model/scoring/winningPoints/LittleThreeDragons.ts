import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class LittleThreeDragons implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_LITTLE_THREE_DRAGONS';
    }

    public getScore(): number {
        return 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}