import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class QuadRobbed implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_QUAD_ROBBED';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}