import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class FullFlush implements WinningPoints {

    private readonly isClosed: boolean;

    public getType(): TWinningPointsType {
        return 'YAKU_CHINITSU';
    }

    public getScore(): number {
        return this.isClosed ? 6 : 5;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    constructor(isClosed: boolean) {
        this.isClosed = isClosed;
    }

}