import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class ThreeParallelTripletsOrKans implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_THREE_PARALLEL_TRIPLETS_OR_KANS';
    }

    public getScore(): number {
        return 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}