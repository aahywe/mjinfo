import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class HalfFlush implements WinningPoints {

    private readonly isClosed: boolean;

    public getType(): TWinningPointsType {
        return 'YAKU_HONITSU';
    }

    public getScore(): number {
        return this.isClosed ? 3 : 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    constructor(isClosed: boolean) {
        this.isClosed = isClosed;
    }

}