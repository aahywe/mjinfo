import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class DuplicateStreets implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_IPPEKU';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}