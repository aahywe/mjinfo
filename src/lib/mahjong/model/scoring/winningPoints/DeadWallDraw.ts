import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class DeadWallDraw implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_DEAD_WALL_DRAW';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}