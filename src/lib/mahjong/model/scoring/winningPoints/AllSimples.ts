import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class AllSimples implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_TANYAO';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}