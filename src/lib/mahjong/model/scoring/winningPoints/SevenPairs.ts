import WinningPoints, {TWinningPointsType} from "../WinningPoints";

import SevenPairsSuiteSet from "@/lib/mahjong/model/suiteSet/SevenPairsSuiteSet";

export default class SevenPairsYaku implements WinningPoints {

    private readonly handConfiguration: SevenPairsSuiteSet;

    public getType(): TWinningPointsType {
        return 'YAKU_SEVEN_PAIRS';
    }

    public getScore(): number {
        return 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    public getHandConfiguration(): SevenPairsSuiteSet {
        return this.handConfiguration;
}

    constructor(handConfiguration: SevenPairsSuiteSet) {
        this.handConfiguration = handConfiguration;
    }
}
