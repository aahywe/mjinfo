import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class Richii implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_RICHII';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}
