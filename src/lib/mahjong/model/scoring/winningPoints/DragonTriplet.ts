import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class DragonTriplet implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_DRAGON_TRIPLET_OR_QUAD';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}