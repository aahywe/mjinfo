import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class CompleteStraight implements WinningPoints {

    private readonly isClosed: boolean;

    public getType(): TWinningPointsType {
        return 'YAKU_ITTSU';
    }

    public getScore(): number {
        return this.isClosed ? 2 : 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    constructor(isClosed: boolean) {
        this.isClosed = isClosed;
    }
}