import WinningPoints, {TWinningPointsType} from "../WinningPoints";

import Tile from "@/lib/mahjong/model/Tile";

export default class UraDora implements WinningPoints {

    private readonly doraTile: Tile;
    private readonly tile: Tile;

    public getScore(): number {
        return 1;
    }

    public getType(): TWinningPointsType {
        return 'YAKU_DORA_URA';
    }

    public getTile(): Tile {
        return this.tile;
    }

    public isSufficientToWin(): boolean {
        return false;
    }

    constructor(doraTile: Tile, tile: Tile) {
        this.doraTile = doraTile;
        this.tile = tile;
    }
}