import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class AllTripletsOrQuads implements WinningPoints {

    private isClosed: boolean;

    public getType(): TWinningPointsType {
        return 'YAKU_ALL_TRIPLETS_OR_QUADS';
    }

    public getScore(): number {
        return this.isClosed ? 3 : 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    constructor(isClosed: boolean) {
        this.isClosed = isClosed;
    }

}