import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class SeatWindPairMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_SEAT_WIND_PAIR';
    }

    public getScore(): number {
        return 2;
    }
}