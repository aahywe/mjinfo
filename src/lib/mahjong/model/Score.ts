/* eslint no-case-declarations: 0 */
import WinningPoints from "./scoring/WinningPoints";

import SuiteSet from "@/lib/mahjong/model/SuiteSet";

import MiniPoints from "@/lib/mahjong/model/scoring/MiniPoints";

/**
 * The list of possible miniPoints (other values get rounded into one of these by a formula)
 */
export type TPossibleMiniPoints = 20 | 25 | 30 | 40 | 50 | 60 | 70 | 80 | 90 | 100 | 110;

/**
 * The table for how much the combination of winningPoints / miniPoints is valued
 * For winningPoints <= 4. (Higher values a calculated differently)
 * null = illegal combination
 */
export const lowerScoresBoard: {
    [winningPoints in 1 | 2 | 3 | 4]: {  // amount of winning points
        [miniPoints in TPossibleMiniPoints]: { // amount of minni points means...
            nonEastPortion: number | null, // points from loosing player on otherDrawn, if player is non-east
            eastPortion: number | null, // points from loosing player on otherDrawn, if player is east
            selfDrawOther: number | null, // points from other player on selfDrawn if player and other player is non-east
            selfDrawEast: number | null // points from other players on selfDrawn if player or other player is east
        } | null
    }
} = {
    1: {
        20: null,
        25: null,
        30: {nonEastPortion: 300, eastPortion: 500, selfDrawOther: null, selfDrawEast: null},
        40: {nonEastPortion: 400, eastPortion: 700, selfDrawOther: 1300, selfDrawEast: 2000},
        50: {nonEastPortion: 400, eastPortion: 800, selfDrawOther: 1600, selfDrawEast: 2400},
        60: {nonEastPortion: 500, eastPortion: 1000, selfDrawOther: 2000, selfDrawEast: 2900},
        70: {nonEastPortion: 600, eastPortion: 1200, selfDrawOther: 2300, selfDrawEast: 3400},
        80: {nonEastPortion: 800, eastPortion: 1300, selfDrawOther: 2600, selfDrawEast: 3900},
        90: {nonEastPortion: 800, eastPortion: 1500, selfDrawOther: 2900, selfDrawEast: 4400},
        100: {nonEastPortion: 800, eastPortion: 1600, selfDrawOther: 3200, selfDrawEast: 4800},
        110: {nonEastPortion: 900, eastPortion: 1800, selfDrawOther: 3600, selfDrawEast: 5300},
    },
    2: {
        20: {nonEastPortion: 400, eastPortion: 700, selfDrawOther: null, selfDrawEast: null},
        25: {nonEastPortion: null, eastPortion: null, selfDrawOther: 1600, selfDrawEast: 2400},
        30: {nonEastPortion: 500, eastPortion: 1000, selfDrawOther: 2000, selfDrawEast: 2900},
        40: {nonEastPortion: 700, eastPortion: 1300, selfDrawOther: 2600, selfDrawEast: 3900},
        50: {nonEastPortion: 800, eastPortion: 1600, selfDrawOther: 3200, selfDrawEast: 4800},
        60: {nonEastPortion: 1000, eastPortion: 2000, selfDrawOther: 3900, selfDrawEast: 5800},
        70: {nonEastPortion: 1200, eastPortion: 2300, selfDrawOther: 4500, selfDrawEast: 6800},
        80: {nonEastPortion: 1300, eastPortion: 2600, selfDrawOther: 5200, selfDrawEast: 7700},
        90: {nonEastPortion: 1500, eastPortion: 2900, selfDrawOther: 5800, selfDrawEast: 8700},
        100: {nonEastPortion: 1600, eastPortion: 3200, selfDrawOther: 6400, selfDrawEast: 9600},
        110: {nonEastPortion: 1800, eastPortion: 3600, selfDrawOther: 7100, selfDrawEast: 10600},
    },
    3: {
        20: {nonEastPortion: 700, eastPortion: 1300, selfDrawOther: null, selfDrawEast: null},
        25: {nonEastPortion: 800, eastPortion: 1600, selfDrawOther: 3200, selfDrawEast: 4800},
        30: {nonEastPortion: 1000, eastPortion: 2000, selfDrawOther: 3900, selfDrawEast: 5800},
        40: {nonEastPortion: 1300, eastPortion: 2600, selfDrawOther: 5200, selfDrawEast: 7700},
        50: {nonEastPortion: 1600, eastPortion: 3200, selfDrawOther: 6400, selfDrawEast: 9600},
        60: {nonEastPortion: 2000, eastPortion: 3900, selfDrawOther: 7700, selfDrawEast: 11600},
        70: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        80: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        90: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        100: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        110: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
    },
    4: {
        20: {nonEastPortion: 1300, eastPortion: 2600, selfDrawOther: null, selfDrawEast: null},
        25: {nonEastPortion: 1600, eastPortion: 3200, selfDrawOther: 6400, selfDrawEast: 9600},
        30: {nonEastPortion: 2000, eastPortion: 3900, selfDrawOther: 7700, selfDrawEast: 11600},
        40: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        50: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        60: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        70: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        80: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        90: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        100: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
        110: {nonEastPortion: 2000, eastPortion: 4000, selfDrawOther: 8000, selfDrawEast: 12000},
    }
};

export default class Score {

    /**
     * The hand configuration belonging to the score
     */
    public readonly handConfiguration: SuiteSet;

    /**
     * A list of all mini points earned
     */
    public readonly miniPoints: Array<MiniPoints>;

    /**
     * A list of all winning points earned
     */
    public readonly winningPoints: Array<WinningPoints>;

    /**
     * The sum of all mini points earned
     */
    get totalMiniPointsScore(): number {
        let totalMiniPointsScore: number = 0;

        for (let miniPoint of this.miniPoints) {
            totalMiniPointsScore += miniPoint.getScore();
        }

        return totalMiniPointsScore;
    }

    /**
     * The sum of all mini points earned, if applicable rounded as you would look them up on the table
     */
    get totalMiniPointsScoreRounded(): TPossibleMiniPoints {
        let totalMiniPointsScore = this.totalMiniPointsScore;

        switch (true) {
            case totalMiniPointsScore === 20:
                return 20;
            case totalMiniPointsScore === 25:
                return 25;
            case totalMiniPointsScore <= 30:
                return 30;
            case totalMiniPointsScore <= 40:
                return 40;
            case totalMiniPointsScore <= 50:
                return 50;
            case totalMiniPointsScore <= 60:
                return 60;
            case totalMiniPointsScore <= 70:
                return 70;
            case totalMiniPointsScore <= 80:
                return 80;
            case totalMiniPointsScore <= 90:
                return 90;
            case totalMiniPointsScore <= 100:
                return 100;
            default: // > 100
                return 110;
        }
    }

    /**
     * The sum of all winning points earned
     */
    get totalWinningPointsScore(): number {
        let totalWinningPointsScore: number = 0;

        for (let winningPoint of this.winningPoints) {
            totalWinningPointsScore += winningPoint.getScore();
        }

        return totalWinningPointsScore;
    }

    /**
     * The amount of points you would get from another player if
     * you robbed the final tile from him and you are east
     */
    get sumOtherDrawnAsEast(): number {
        let winningPoints = this.totalWinningPointsScore;
        let miniPoints = this.totalMiniPointsScoreRounded;

        switch (winningPoints) {
            case 1:
            case 2:
            case 3:
            case 4:
                let entry = lowerScoresBoard[winningPoints][miniPoints];
                if (entry === null || entry.selfDrawEast === null) {
                    throw new RangeError(
                        'Score.ts/sumOtherDrawnAsEast: Illegal combination - '
                        + winningPoints
                        + ' with '
                        + miniPoints
                    );
                }
                return entry.selfDrawEast;

            case 5:
                return 12000;
            case 6:
            case 7:
                return 16000;
            case 8:
            case 9:
            case 10:
                return 24000;
            case 11:
            case 12:
                return 32000;
            default: // > 12
                return 48000;
        }
    }

    /**
     * The amount of points you would get from another player if
     * you robbed the final tile from him and you are non-east
     */
    get sumOtherDrawnAsNonEast(): number {
        let winningPoints = this.totalWinningPointsScore;
        let miniPoints = this.totalMiniPointsScoreRounded;

        switch (winningPoints) {
            case 1:
            case 2:
            case 3:
            case 4:
                let entry = lowerScoresBoard[winningPoints][miniPoints];
                if (entry === null || entry.selfDrawOther === null) {
                    throw new RangeError(
                        'Score.ts/sumOtherDrawnAsNonEast: Illegal combination - '
                        + winningPoints
                        + ' with '
                        + miniPoints
                    );
                }
                return entry.selfDrawOther;

            case 5:
                return 8000;
            case 6:
            case 7:
                return 12000;
            case 8:
            case 9:
            case 10:
                return 16000;
            case 11:
            case 12:
                return 24000;
            default: // > 12
                return 32000;
        }
    }

    /**
     * The amount of points you would get from other players when you
     * drew the tile yourself and you or the other player is east
     */
    get sumSelfDrawnAsOrAgainstEast(): number {
        let winningPoints = this.totalWinningPointsScore;
        let miniPoints = this.totalMiniPointsScoreRounded;

        switch (winningPoints) {
            case 1:
            case 2:
            case 3:
            case 4:
                let entry = lowerScoresBoard[winningPoints][miniPoints];
                if (entry === null || entry.nonEastPortion === null) {
                    throw new RangeError(
                        'Score.ts/sumSelfDrawnAsOrAgainstEast: Illegal combination - '
                        + winningPoints
                        + ' with '
                        + miniPoints
                    );
                }
                return entry.nonEastPortion;

            case 5:
                return 2000;
            case 6:
            case 7:
                return 3000;
            case 8:
            case 9:
            case 10:
                return 4000;
            case 11:
            case 12:
                return 6000;
            default: // > 12
                return 8000;
        }
    }

    /**
     * The amount of points you would get from other players when you
     * drew the tile yourself and you and the other are non-east
     */
    get sumSelfDrawnAsAndAgainstOther(): number {
        let winningPoints = this.totalWinningPointsScore;
        let miniPoints = this.totalMiniPointsScoreRounded;

        switch (winningPoints) {
            case 1:
            case 2:
            case 3:
            case 4:
                let entry = lowerScoresBoard[winningPoints][miniPoints];
                if (entry === null || entry.eastPortion === null) {
                    throw new RangeError(
                        'Score.ts/sumSelfDrawnAsAndAgainstOther: Illegal combination - '
                        + winningPoints
                        + ' with '
                        + miniPoints
                    );
                }
                return entry.eastPortion;

            case 5:
                return 4000;
            case 6:
            case 7:
                return 6000;
            case 8:
            case 9:
            case 10:
                return 8000;
            case 11:
            case 12:
                return 12000;
            default: // > 12
                return 16000;
        }
    }

    /**
     * Represents the total score
     * @param handConfiguration - The hand that has been calculated
     * @param miniPoints - The list of minni points earned
     * @param winningPoints - The list of winning points earned
     */
    constructor(
        handConfiguration: SuiteSet,
        miniPoints: Array<MiniPoints>,
        winningPoints: Array<WinningPoints>,
    ) {
        if (miniPoints.length === 0) {
            throw new RangeError('Score.ts/miniPoints: Too few mini points');
        }

        if (winningPoints.length === 0) {
            throw new RangeError('Score.ts/miniPoints: Too few winning points');
        }

        this.handConfiguration = handConfiguration;
        this.miniPoints = miniPoints;
        this.winningPoints = winningPoints;
    }
}