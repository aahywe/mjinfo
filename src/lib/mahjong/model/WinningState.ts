export type TFinalTileDrawnState = 'selfDrawn' | 'otherDrawn' | 'quadSupplementTileDrawn' | 'quadRobbedDrawn';
export type TRichiiState = 'without' | 'with' | 'withDouble' | 'with_immediate' | 'withDouble_immediate';
export type TSeatWind = 'east' | 'south' | 'west' | 'north';
export type TRoundWind = 'east' | 'south';

export default class WinningState {

    private finalTileDrawnState: TFinalTileDrawnState;
    private richiiState: TRichiiState;
    private seatWind: TSeatWind;
    private roundWind: TRoundWind;
    private lastTile: boolean;

    public getFinalTileDrawnState(): TFinalTileDrawnState {
        return this.finalTileDrawnState;
    }

    public setFinalTileDrawnState(newState: TFinalTileDrawnState): void {
        this.finalTileDrawnState = newState;
    }

    public getRichiiState(): TRichiiState {
        return this.richiiState;
    }

    public setRichiiState(newState: TRichiiState): void {
        this.richiiState = newState;
    }

    public getSeatWind(): TSeatWind {
        return this.seatWind;
    }

    public setSeatWind(newState: TSeatWind): void {
        this.seatWind = newState;
    }

    public getRoundWind(): TRoundWind {
        return this.roundWind;
    }

    public getLastTile(): boolean {
        return this.lastTile;
    }

    public setLastTile(newValue: boolean) {
        this.lastTile = newValue;
    }

    public setRoundWind(newState: TRoundWind): void {
        this.roundWind = newState;
    }

    constructor(
        finalTileDrawnState: TFinalTileDrawnState,
        richiiState: TRichiiState,
        seatWind: TSeatWind,
        roundWind: TRoundWind,
        lastTile: boolean,
    ) {
        this.finalTileDrawnState = finalTileDrawnState;
        this.richiiState = richiiState;
        this.seatWind = seatWind;
        this.roundWind = roundWind;
        this.lastTile = lastTile;
    }
}