import Quad from "@/lib/mahjong/model/suites/Quad";
import Triplet from "@/lib/mahjong/model/suites/Triplet";
import Pair from "@/lib/mahjong/model/suites/Pair";
import Street from "@/lib/mahjong/model/suites/Street";

import SuiteSet, {TSuiteSetType} from "@/lib/mahjong/model/SuiteSet";
import Tile from "@/lib/mahjong/model/Tile";

export default class SevenPairsSuiteSet implements SuiteSet {

    private readonly pairs: Array<Pair>;

    /**
     * The pair suites in this set
     */
    public getPairs(): Array<Pair> {
        return this.pairs;
    }

    /**
     * The street suites in this set (of which this suite set has none)
     */
    public getStreets(): Array<Street> {
        return [];
    }

    /**
     * The triplet suites in this set (of which this suite set has none)
     */
    public getTriplets(): Array<Triplet> {
        return [];
    }

    /**
     * The quad suite in this set (of which this suite set has none)
     */
    public getQuads(): Array<Quad> {
        return [];
    }

    public getType(): TSuiteSetType {
        return 'HAND_CONFIGURATION_SEVEN_PAIRS';
    }

    /**
     * All tiles in all suites
     */
    public getAllTiles(): Array<Tile> {
        let allTiles: Array<Tile> = [];
        for (let pair of this.pairs) {
            allTiles.push(pair.firstTile);
            allTiles.push(pair.secondTile);
        }

        return allTiles;
    }

    /**
     * True if the suite is completely closed
     * (this is a pairs only suite set, and pairs are always closed)
     */
    public isClosed(): boolean {
        return true;
    }

    constructor(pairs: Array<Pair>) {
        if (pairs.length !== 7) {
            throw new RangeError('SevenPairsSuiteSet.ts/constructor: Invalid number of pairs');
        }

        this.pairs = pairs;
    }
}
