import Quad from "@/lib/mahjong/model/suites/Quad";
import Triplet from "@/lib/mahjong/model/suites/Triplet";
import Pair from "@/lib/mahjong/model/suites/Pair";
import Street from "@/lib/mahjong/model/suites/Street";

import SuiteSet, {TSuiteSetType} from "@/lib/mahjong/model/SuiteSet";
import Tile from "@/lib/mahjong/model/Tile";

export default class StandardSuiteSet implements SuiteSet {

    private readonly pair: Pair;
    private readonly streets: Array<Street>;
    private readonly triplets: Array<Triplet>;
    private readonly quads: Array<Quad>;

    /**
     * The pair suites in this set
     */
    public getPairs(): Array<Pair> {
        return [this.pair];
    }

    /**
     * The street suites in this set
     */
    public getStreets(): Array<Street> {
        return this.streets;
    }

    /**
     * The triplet suites in this set
     */
    public getTriplets(): Array<Triplet> {
        return this.triplets;
    }

    /**
     * The quad suite in this set
     */
    public getQuads(): Array<Quad> {
        return this.quads;
    }

    /**
     * All tiles in all suites
     */
    public getType(): TSuiteSetType {
        return 'HAND_CONFIGURATION_NORMAL';
    }

    /**
     * True if the suite is completely closed
     */
    public isClosed(): boolean {
        for (let street of this.streets) {
            if (street.getIsOpen()) {
                return false;
            }
        }

        for (let triplet of this.triplets) {
            if (triplet.getIsOpen()) {
                return false;
            }
        }

        for (let quad of this.quads) {
            if (quad.getIsOpen()) {
                return false;
            }
        }

        return true;
    }

    /**
     * All tiles in all suites
     */
    public getAllTiles(): Array<Tile> {
        let allTiles: Array<Tile> = [this.pair.firstTile, this.pair.secondTile];
        
        for (let street of this.streets) {
            allTiles.push(street.lowestTile);
            allTiles.push(street.midTile);
            allTiles.push(street.upperTile);
        }

        for (let triplet of this.triplets) {
            allTiles.push(triplet.firstTile);
            allTiles.push(triplet.secondTile);
            allTiles.push(triplet.thirdTile);
        }

        for (let quad of this.quads) {
            allTiles.push(quad.firstTile);
            allTiles.push(quad.secondTile);
            allTiles.push(quad.thirdTile);
            allTiles.push(quad.fourthTile);
        }

        return allTiles;
    }

    /**
     * Represents the tiles of a hand of a player
     * Represent a standard set of suites (4 x street/triplet/quad and 1 x pair)
     * @param pair
     * @param streets
     * @param triplets
     * @param quads
     */
    constructor(
        pair: Pair,
        streets: Array<Street>,
        triplets: Array<Triplet>,
        quads: Array<Quad>,
    ) {
        if (streets.length + triplets.length + quads.length !== 4) {
            throw new RangeError('StandardSuiteSet.ts/constructor: Invalid amount of suites');
        }

        this.pair = pair;
        this.streets = streets;
        this.triplets = triplets;
        this.quads = quads;
    }

}