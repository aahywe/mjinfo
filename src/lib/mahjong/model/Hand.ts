import Tile from "@/lib/mahjong/model/Tile";

import Quad from "@/lib/mahjong/model/suites/Quad";
import Triplet from "@/lib/mahjong/model/suites/Triplet";
import Street from "@/lib/mahjong/model/suites/Street";

export default class Hand {

    /*
     * The stack of tiles hidden from other players
     */
    public readonly closedTiles: Array<Tile>;

    /**
     * Quad suites that have been revealed, but still count as closed
     */
    public readonly closedQuads: Array<Quad>;

    /**
     * Triplet suites that have been revealed
     */
    public readonly openTriplets: Array<Triplet>;

    /**
     * Quad suites that have been revealed
     */
    public readonly openQuads: Array<Quad>;

    /**
     * Street suites that have been revealed
     */
    public readonly openStreets: Array<Street>;

    /**
     * The very final tile that is other self drawn or robbed from other players
     */
    public readonly extraTile: Tile;

    /**
     * Represents a players hand during his turn (i.e. including the tile just drawn or robbed)
     * @param closedTiles - The stack of tiles hidden from other players
     * @param closedQuads - Quad suites that have been revealed, but still count as closed
     * @param openTriplets - Triplet suites that have been revealed
     * @param openQuads - Quad suites that have been revealed
     * @param openStreets - Street suites that have been revealed
     * @param extraTile - The very final tile that is other self drawn or robbed from other players
     */
    constructor(
        closedTiles: Array<Tile>,
        closedQuads: Array<Quad>,
        openTriplets: Array<Triplet>,
        openQuads: Array<Quad>,
        openStreets: Array<Street>,
        extraTile: Tile,
    ) {
        for (let closedQuad of closedQuads) {
            if (closedQuad.getSuiteType() !== 'kan' || closedQuad.getIsOpen()) {
                throw new RangeError('Hand.ts/constructor: Invalid closedQuad');
            }
        }

        for (let openTriplet of openTriplets) {
            if (openTriplet.getSuiteType() !== 'pon' || !openTriplet.getIsOpen()) {
                throw new RangeError('Hand.ts/constructor: Invalid openTriplet');
            }
        }

        for (let openQuad of openQuads) {
            if (openQuad.getSuiteType() !== 'kan' || openQuad.getIsOpen()) {
                throw new RangeError('Hand.ts/constructor: Invalid openQuad');
            }
        }

        for (let openStreet of openStreets) {
            if (openStreet.getSuiteType() !== 'kan' || openStreet.getIsOpen()) {
                throw new RangeError('Hand.ts/constructor: Invalid openStreet');
            }
        }

        let closedTilesExpected: number = 13
            - 3 * openQuads.length
            - 3 * closedQuads.length
            - 3 * openTriplets.length
            - 3 * openStreets.length;

        if (closedTiles.length !== closedTilesExpected) {
            throw new RangeError('Hand.ts/constructor: Unexpected amount of closed tiles');
        }

        this.closedTiles = closedTiles;
        this.closedQuads = closedQuads;
        this.openTriplets = openTriplets;
        this.openQuads = openQuads;
        this.openStreets = openStreets;
        this.extraTile = extraTile;
    }
}
