import {de} from "@/assets/i18n/language/de";
import {en} from "@/assets/i18n/language/en";

import {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";
import {TWinningPointsType} from "@/lib/mahjong/model/scoring/WinningPoints";

/**
 * All language keys
 */
export type TLanguageKeys =
    TMiniPointsType // Also add miniPoint reward names
    | TWinningPointsType // Also add winningPoint reward names
    | 'APP_NAV_HOME'
    | 'APP_NAV_CALCULATOR'
    | 'APP_NAV_WINNING_HANDS'
    | 'APP_NAV_SCORE_TABLE'
    | 'CALCULATOR_HEADER'
    | 'CALCULATOR_WIDGET_TILES_CLOSED'
    | 'CALCULATOR_WIDGET_TILES_GROUP1'
    | 'CALCULATOR_WIDGET_TILES_GROUP2'
    | 'CALCULATOR_WIDGET_TILES_GROUP3'
    | 'CALCULATOR_WIDGET_TILES_GROUP4'
    | 'CALCULATOR_WIDGET_TILES_EXTRA'
    | 'CALCULATOR_WIDGET_GROUP_EXPLANATION'
    | 'CALCULATOR_WIDGET_GROUP_EXPLANATION_EXTRA'
    | 'CALCULATOR_WIDGET_TILES_REMAINING'
    | 'CALCULATOR_WIDGET_SELECT_TILES'
    | 'CALCULATOR_WIDGET_SELECT_WINNING_TILES'
    | 'CALCULATOR_WIDGET_RESULT'
    | 'CALCULATOR_WIDGET_TILES_DORA'
    | 'CALCULATOR_WIDGET_TILES_URA_DORA'
    | 'CALCULATOR_RESULT_ROUNDED'
    | 'CALCULATOR_RESULT_TOTAL'
    | 'CALCULATOR_RESULT_MINI_POINTS'
    | 'CALCULATOR_RESULT_WINNING_POINTS'
    | 'CALCULATOR_RESULT_SELF_DRAWN'
    | 'CALCULATOR_RESULT_OTHER_DRAWN'
    | 'CALCULATOR_RESULT_EAST'
    | 'CALCULATOR_RESULT_SOUTH_WEST_NORTH'
    | 'CALCULATOR_RESULT_AGAINST_ALL'
    | 'CALCULATOR_RESULT_AGAINST_EAST'
    | 'CALCULATOR_RESULT_AGAINST_OTHER'
    | 'EXTRA_INFO_DRAWN'
    | 'EXTRA_INFO_RICHII'
    | 'EXTRA_INFO_SELF_DRAWN'
    | 'EXTRA_INFO_OTHER_DRAWN'
    | 'EXTRA_INFO_NO_RICHII'
    | 'EXTRA_INFO_WITH_RICHII'
    | 'EXTRA_INFO_WITH_DOUBLE_RICHII'
    | 'EXTRA_INFO_RICHII_IMMEDIATE'
    | 'EXTRA_INFO_DOUBLE_RICHII_IMMEDIATE'
    | 'EXTRA_INFO_SEAT_WIND'
    | 'EXTRA_INFO_SEAT_WIND_EAST'
    | 'EXTRA_INFO_SEAT_WIND_SOUTH'
    | 'EXTRA_INFO_SEAT_WIND_WEST'
    | 'EXTRA_INFO_SEAT_WIND_NORTH'
    | 'EXTRA_INFO_ROUND_WIND'
    | 'EXTRA_INFO_LAST_TILE_INFO'
    | 'EXTRA_INFO_LAST_TILE'
    | 'EXTRA_INFO_NOT_LAST_TILE'
    | 'EXTRA_INFO_QUAD_SUPPLEMENT_DRAWN'
    | 'EXTRA_INFO_QUAD_ROBBED_DRAWN'
    | 'WINNING_HANDS_POINT'
    | 'WINNING_HANDS_POINTS'
    | 'WINNING_HANDS_OR'
    | 'WINNING_HANDS_POINTS_PLUS_IF_CLOSED'
    | 'WINNING_HANDS_POINTS_CLOSED'
    | 'WINNING_HANDS_HEADER'
    | 'WINNING_HANDS_FLUSH_HEADER'
    | 'WINNING_HANDS_FLUSH_HALF_FLUSH_HEADER'
    | 'WINNING_HANDS_FLUSH_HALF_FLUSH_DESCRIPTION'
    | 'WINNING_HANDS_FLUSH_FULL_FLUSH_HEADER'
    | 'WINNING_HANDS_FLUSH_FULL_FLUSH_DESCRIPTION'
    | 'WINNING_HANDS_TILE_TYPE_HEADER'
    | 'WINNING_HANDS_TILE_TYPE_ALL_SIMPLE_HEADER'
    | 'WINNING_HANDS_TILE_TYPE_ALL_SIMPLE_DESCRIPTION'
    | 'WINNING_HANDS_TILE_TYPE_TERMINAL_OR_HONOR_HEADER'
    | 'WINNING_HANDS_TILE_TYPE_TERMINAL_OR_HONOR_DESCRIPTION'
    | 'WINNING_HANDS_TILE_TYPE_TERMINAL_HEADER'
    | 'WINNING_HANDS_TILE_TYPE_TERMINAL_DESCRIPTION'
    | 'WINNING_HANDS_STREET_BASED_HEADER'
    | 'WINNING_HANDS_STREET_BASED_ONE_TWIN_HEADER'
    | 'WINNING_HANDS_STREET_BASED_ONE_TWIN_DESCRIPTION'
    | 'WINNING_HANDS_STREET_BASED_TWO_TWINS_HEADER'
    | 'WINNING_HANDS_STREET_BASED_TWO_TWINS_DESCRIPTION'
    | 'WINNING_HANDS_STREET_BASED_THREE_STREETS_HEADER'
    | 'WINNING_HANDS_STREET_BASED_THREE_STREETS_DESCRIPTION'
    | 'WINNING_HANDS_STREET_BASED_BIG_STREET_HEADER'
    | 'WINNING_HANDS_STREET_BASED_BIG_STREET_DESCRIPTION'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_HEADER'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_ALL_TRIPLETS_HEADER'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_ALL_TRIPLETS_DESCRIPTION'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_THREE_CLOSED_TRIPLETS_HEADER'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_THREE_CLOSED_TRIPLETS_DESCRIPTION'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_THREE_KANS_HEADER'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_THREE_KANS_DESCRIPTION'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_THREE_EQUAL_TRIPLETS_HEADER'
    | 'WINNING_HANDS_TRIPLET_QUAD_BASED_THREE_EQUAL_TRIPLETS_DESCRIPTION'
    | 'WINNING_HANDS_OTHER_BASED_HEADER'
    | 'WINNING_HANDS_TRIPLET_OTHER_BASED_NO_MINIPOINTS_HEADER'
    | 'WINNING_HANDS_TRIPLET_OTHER_BASED_NO_MINIPOINTS_DESCRIPTION'
    | 'WINNING_HANDS_TRIPLET_OTHER_BASED_LITTLE_THREE_DRAGONS_HEADER'
    | 'WINNING_HANDS_TRIPLET_OTHER_BASED_LITTLE_THREE_DRAGONS_DESCRIPTION'
    | 'SCORE_TABLE_HEADER'
    | 'SCORE_TABLE_LESS_THEN_5'
    | 'SCORE_TABLE_LESS_THEN_5_HEADER'
    | 'SCORE_TABLE_MORE_THEN_5'
    | 'SCORE_TABLE_MORE_THEN_5_HEADER'
    | 'SCORE_TABLE_SELF_DRAWN'
    | 'SCORE_TABLE_OTHER_DRAWN'
    | 'SCORE_TABLE_NAME'
    | 'SCORE_TABLE_NON_EAST'
    | 'SCORE_TABLE_EAST'
    | 'SCORE_TABLE_MANGAN'
    | 'SCORE_TABLE_HANEMAN'
    | 'SCORE_TABLE_BAIMAN'
    | 'SCORE_TABLE_SAN_BAIMAN'
    | 'SCORE_TABLE_YAKUMAN'
    | 'HOME_HEADLINE'
    | 'HOME_SUBHEADLINE'
    | 'HOME_THANKS'
    | 'HOME_DISCLAIMER'
    ;

/**
 * Keys of all languages
 */
export type TLanguages = 'en' | 'de'

/**
 * Key/Value pair for all language keys => text
 */
export type TLanguageData = {[key in TLanguageKeys]: string};

/**
 * All data for one language
 */
export type TLanguage = {'id': string, 'data': TLanguageData}

/**
 * Offers translated strings for keywords. Singelton pattern
 */
export default class Language {
    private static instance: Language = new Language();

    /**
     * Returns the singelton instance
     */
    public static getInstance(): Language {
        return this.instance;
    }

    /**
     * The language currently active
     */
    private currentLanguage: TLanguage = en;

    /**
     * Gets the key of the language currently installed
     */
    get currentLanguageId(): string {
        return this.currentLanguage.id;
    }

    /**
     * Switches the language
     * @param language
     */
    public setLanguage(language: TLanguages): void {
        switch (language) {
            case 'en':
                this.currentLanguage = en;
                break;

            case 'de':
                this.currentLanguage = de;
                break;
        }
    }

    /**
     * Returns the text for a language key
     */
    public get(keyword: TLanguageKeys): string {
        let result: string = this.currentLanguage.data[keyword];

        if (result === undefined || result === null) {
            throw new RangeError('Language.ts/get: Language information not set for ' + keyword);
        }

        return result;
    }
}